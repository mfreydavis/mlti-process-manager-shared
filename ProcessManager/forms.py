__author__ = 'matt'

from django import forms
from ProcessManager.models import Assignment, CheckIn, DeviceCheckIn, AccessoryCheckIn, CheckInNote
from django.forms.models import modelformset_factory, BaseModelFormSet


class DataUploadForm(forms.Form):
    CHOICES=(
        ('Person', 'People'),
        ('DeviceAssignments', 'Device Assignments'),
        ('Assets', 'Assets'),
        ('DeviceType', 'Device Types')
    )

    upload_type = forms.ChoiceField(choices=CHOICES, required=True, help_text='Select the type of upload.')

    file = forms.FileField(
        widget=forms.FileInput(attrs={'id' : 'id_select_person_upload',
                                      'placeholder' : 'Upload a file.',
                                      'class' : 'form-control input-lg'})

    )


class AssignmentForm(forms.models.ModelForm):

    class Meta:
        model = Assignment
        fields = ('end_date',)


class CheckInForm(forms.models.ModelForm):

    class Meta:
        model = CheckIn
        fields = ('start_date', 'end_date', 'note')
        widgets= {
            'start_date': forms.TextInput(attrs={'class': 'form-control date_picker'}),
            'end_date': forms.TextInput(attrs={'class': 'form-control date_picker'}),
            'note': forms.Textarea(attrs={'class': 'form-control', 'rows': 2})
          }



class DeviceCheckInForm(forms.models.ModelForm):
    status = forms.ChoiceField(widget=forms.RadioSelect(attrs={'class': 'choice_check'}), choices=AccessoryCheckIn.STATUS_CHOICES, )

    class Meta:
        model= DeviceCheckIn
        fields = {'status', 'warranty'}
        attrs= {'warranty' : {'class': 'choice_check'}}


class AccessoryCheckInForm(forms.models.ModelForm):
    status = forms.ChoiceField(widget=forms.RadioSelect(attrs={'class': 'choice_check'}), choices=AccessoryCheckIn.STATUS_CHOICES, )

    class Meta:
        model = AccessoryCheckIn
        fields = {'status', 'warranty', 'id'}
        attrs= {'warranty' : {'class': 'choice_check'}}

        #widgets = {'warranty': forms.BooleanField(attrs={'class': 'choice_check'}),}


class CheckInNoteForm(forms.models.ModelForm):

    class Meta:
        model = CheckInNote
        fields = {'content' }
        widgets= {'content': forms.Textarea(attrs={'class': 'form-control', 'rows': 5})}






