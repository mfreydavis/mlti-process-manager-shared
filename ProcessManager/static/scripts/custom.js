/**
 * Created by matt on 8/20/14.
 */



jQuery(document).ready(function($) {
      $(".clickable_row").click(function() {
            window.document.location = $(this).data('url');
      });

    $( ".date_picker" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm/dd/yy"
    });
    $("div.btn-toolbar > div > label").addClass('btn btn-primary')
    $("#id_check_in_table input[class=choice_check]:checked").parent().addClass("active")
    $("#id_check_in_table label[name=warranty_check] > input:checked").parent().addClass("active")
    });





$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('assignment_id') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient)
});


