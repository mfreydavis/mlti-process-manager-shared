/**
 * Created by matt on 5/13/15.
 */

// Do the vendor prefix dance
navigator.getUserMedia  = navigator.getUserMedia    || navigator.webkitGetUserMedia ||
                          navigator.mozGetUserMedia || navigator.msGetUserMedia;

// Set up an error handler on the callback
var errCallback = function(e) {
  console.log('Did you just reject me?!', e);
};

// Request the user's media
function requestMedia(e) {
  e.preventDefault();

// Use the vendor prefixed getUserMedia we set up above and request just video
  navigator.getUserMedia({video: true, audio: false}, showMedia, errCallback);
}

// Actually show the media
function showMedia(stream) {
  var video = document.getElementById('user-media');
  video.src = window.URL.createObjectURL(stream);

video.onloadedmetadata = function(e) {
    console.log('Locked and loaded.');
  };
}



// Set up a click handler to kick off the process
$(function() {
  $('#get-user-media').click(requestMedia);
});


function capture(video, canvas, image, snapshotButton) {

	var constraints = {
		video: true
	}

	var successCallback = function(mediaStream) {
		video.src = window.URL.createObjectURL(mediaStream);
		video.addEventListener("loadedmetadata", function(e) {
			snapshotButton.onclick = function() {
				takePhoto();
			}

		}
	};

	var errorCallback = function() {
		console.log('failure to get media');
	};

	var takePhoto = function () {
		var ctx = canvas.getContext('2d');
		ctx.drawImage(video,0,0);
		showImage();
	};

	var showImage = function () {
		image.src = canvas.toDataURL('image/webp');
	};

	navigator.getUserMedia(constraints, successCallback, errorCallback);

}

//From
//http://blog.zencoder.com/2014/07/28/html5-video-and-the-end-of-plugins-recording-uploading-and-transcoding-video-straight-from-the-browser/