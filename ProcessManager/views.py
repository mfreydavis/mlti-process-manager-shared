from django.shortcuts import render, redirect
from django.core import exceptions
from django.http import HttpResponse, HttpResponseRedirect
from ProcessManager.models import Asset, Person, Assignment, Accessory, CheckIn, DeviceCheckIn, AccessoryCheckIn, CheckInNote
from ProcessManager.forms import DataUploadForm, AssignmentForm, CheckInForm, \
    DeviceCheckInForm, AccessoryCheckInForm, CheckInNoteForm
from ProcessManager.utilities import person_csv, device_csv, asset_csv, assignments_csv
from django.utils import timezone
from django.forms.models import modelformset_factory

# Create your views here.


def home_page(request):
    form = CheckInForm()
    checkins = CheckIn.objects.all()
    return render(request, 'ProcessManager/home.html', {'form': form,
                                                        'checkins': checkins})

def new_check_in(request):
    form = CheckInForm
    if request.method == 'POST':
        form = CheckInForm(request.POST)
        if form.is_valid():
            form.save()

    return redirect('/')

def existing_check_in(request, checkin_id):
    check = CheckIn.objects.get(pk=checkin_id)
    CheckInResultsInProgress = DeviceCheckIn.objects.filter(check_in=check)
    AccessoryCheckInProgress = AccessoryCheckIn.objects.filter(check_in=check)

    ProgressResults = []
    for result in CheckInResultsInProgress:
        accumulator = []
        for acc in AccessoryCheckInProgress:
            if result.assignment == acc.assignment:
                accumulator.append(acc)
        ProgressResults.append({'device': result, 'accessory': accumulator})






    context = {'check_in': check,
               'CheckInResultsInProgress': CheckInResultsInProgress,
               'ProgressResults': ProgressResults}



    return render(request, 'ProcessManager/checkin.html', context)


def checkin_search_person(request, check_in):
    criteria = request.POST.get('last_name', '')
    context = {'criteria': criteria, 'check_in': check_in}
    found_person = Person()

    if len(criteria) == 0:
        return redirect('checkin_select_person', context['check_in'])

    if criteria.isnumeric():
        try:
            found_person = Person.objects.get(student_number=criteria)
        except (exceptions.MultipleObjectsReturned, found_person.DoesNotExist):
            return redirect('checkin_select_person', context['check_in'], context['criteria'])

    else:
        try:
            found_person = Person.objects.get(last_name=criteria)
        except (exceptions.MultipleObjectsReturned, found_person.DoesNotExist):
            return redirect('checkin_select_person', context['check_in'], context['criteria'])

    assignments = Assignment.objects.filter(person=found_person)

    if assignments.count() > 1:
        return redirect('checkin_select_person', context['check_in'], context['check_in'])

    if assignments.count() == 0:
        return redirect('checkin_select_person', context['check_in'], context['criteria'])


    return redirect('/checkin/{0}/{1}'.format(check_in, assignments[0].id), context)


def checkin_select_person(request, check_in, criteria=''):
    people = Person.objects.all().filter(last_name__contains=criteria)
    check = CheckIn.objects.get(pk=check_in)
    if people.count() == 0:
        people = Person.objects.all()

    return render(request,
                  'ProcessManager/checkin_select_person.html',
                  {'persons_information': people,
                   'check_in': check},
    )




def device_check_in(request, checkin_id, assignment_id):
    check = CheckIn.objects.get(pk=checkin_id)
    assignment = Assignment.objects.get(pk=assignment_id)
    person_information = Person.objects.get(pk=assignment.person.id)
    accessory_information = Accessory.objects.filter(asset=assignment.asset)

    # Get an existing device check if possible.

    device_check, created = DeviceCheckIn.objects.get_or_create(check_in=check,
                                                                assignment=assignment,
                                                                )
    for accessory in accessory_information:
        AccessoryCheckIn.objects.get_or_create(check_in=check,
                                               assignment=assignment,
                                               accessory=accessory)

    existing_accessory_check_in = AccessoryCheckIn.objects.filter(check_in=check,
                                                                  assignment=assignment)

    AccessoryCheckInFormSet = modelformset_factory(AccessoryCheckIn, form=AccessoryCheckInForm, extra=0)
    AccessoryFormSet = AccessoryCheckInFormSet(queryset=existing_accessory_check_in, prefix='accessories')

    check_note, created = CheckInNote.objects.get_or_create(assignment=assignment, check_in=check)

    NoteForm = CheckInNoteForm(instance=check_note, prefix='notes')
    DeviceForm = DeviceCheckInForm(instance=device_check, prefix='devices')

    CheckInResultsInProgress = DeviceCheckIn.objects.filter(check_in=check)
    AccessoryCheckInProgress = AccessoryCheckIn.objects.filter(check_in=check)

    ProgressResults = []
    for result in CheckInResultsInProgress:
        accumulator = []
        for acc in AccessoryCheckInProgress:
            if result.assignment == acc.assignment:
                accumulator.append(acc)
        ProgressResults.append({'device': result, 'accessory': accumulator})

    context = {'check_in': check,
               'person_information': person_information,
               'assignment_information' : assignment,
               'accessory_information' : accessory_information,
               'DeviceForms': DeviceForm,
               'AccessoryFormSet': AccessoryFormSet,
               'NoteForm': NoteForm,
               'ProgressResults': ProgressResults}

    return render(request, 'ProcessManager/checkin.html', context)


def save_check_in_results(request, check_in_id, assignment_id):
    check = CheckIn.objects.get(pk=check_in_id)
    assignment = Assignment.objects.get(pk=assignment_id)
    AccessoryCheckInFormSet = modelformset_factory(AccessoryCheckIn, form=AccessoryCheckInForm, extra=0)
    device_check_in = DeviceCheckIn.objects.get(check_in=check, assignment=assignment)
    note_check_in = CheckInNote.objects.get(check_in=check, assignment=assignment)



    if request.method == 'POST':
        device_check = DeviceCheckInForm(request.POST, instance=device_check_in, prefix='devices')
        note_check = CheckInNoteForm(request.POST, instance=note_check_in, prefix='notes')
        accessory_check = AccessoryCheckInFormSet(request.POST, prefix='accessories')

        if device_check.is_valid():
            device_check.save()
        if note_check.is_valid():
            note_check.save()
        if accessory_check.is_valid():
            accessory_check.save()

    return redirect('/checkin/{0}/{1}'.format(check.id, assignment.id))

def search_asset(request):
    asset_tag = request.POST.get('asset_tag', '')

    if not asset_tag.isnumeric() or len(asset_tag) == 0:
        return redirect('/')

    asset_ = Asset.objects.all().filter(tag=asset_tag)

    if asset_.count() == 0:
        return redirect('/')

    return redirect('/asset/%s/' % (asset_tag,))


def asset(request, asset_tag):
    found_asset = Asset.objects.get(tag=asset_tag)
    all_assignments = Assignment.objects.all().filter(asset=found_asset)
    accessories = Accessory.objects.filter(asset=found_asset)
    assignment = [asset for asset in all_assignments if asset.active]
    return render(request,
                  'ProcessManager/asset.html',
                  {'asset_information': found_asset,
                   'assignment_information': assignment,
                   'accessories' : accessories}
    )


def add_accessory(request, asset_tag):
    if request.method == 'POST':
        found_asset = Asset.objects.get(tag=asset_tag)
        if request.POST.get('accessory_name') == '':
            pass
        else:
            Accessory.objects.create(name=request.POST.get('accessory_name'), asset=found_asset)

    return redirect('/asset/%s/' % (asset_tag,))


def remove_accessory(request, asset_tag):
    if request.method == 'POST':
        found_asset = Asset.objects.get(tag=asset_tag)
        if request.POST.get('accessory_id') != '':
            Accessory.objects.get(pk=request.POST.get('accessory_id'), asset=found_asset).delete()

    return redirect('/asset/%s/' % (asset_tag,))


def search_person(request):
    criteria = request.POST.get('last_name', '')
    found_person = Person()

    if len(criteria) == 0:
        return redirect('all_people')

    if criteria.isnumeric():
        try:
            found_person = Person.objects.get(student_number=criteria)
        except (exceptions.MultipleObjectsReturned, found_person.DoesNotExist):
            return redirect('select_person', criteria)

    else:
        try:
            found_person = Person.objects.get(last_name=criteria)
        except (exceptions.MultipleObjectsReturned, found_person.DoesNotExist):
            return redirect('select_person', criteria)

    return redirect('/person/%s/' % (found_person.id,))


def select_person(request, criteria=''):
    people = Person.objects.all().filter(last_name__contains=criteria)
    if people.count() == 0:
        people = Person.objects.all()

    return render(request,
                  'ProcessManager/select_person.html',
                  {'persons_information': people},
    )


def person(request, person_id):
    found_person = Person.objects.get(id=person_id)
    found_assignments = Assignment.objects.all().filter(person=found_person)
    if found_assignments.count() > 0:
        forms = [AssignmentForm(instance=assignment) for assignment in found_assignments]
    else:
        forms = [AssignmentForm()]
    return render(request,
                  'ProcessManager/person.html',
                  {'person_information': found_person, 'AssignmentForms': forms}
    )


def import_files(request):

    if request.method == 'POST':
        form = DataUploadForm(request.POST, request.FILES)
        if form.is_valid():
            choice = request.POST['upload_type']
            if choice == 'Person':
                results = person_csv.handle_uploaded_file(request.FILES['file'])
            elif choice == 'DeviceType':
                results = device_csv.handle_uploaded_file(request.FILES['file'])
            elif choice == 'Assets':
                results = asset_csv.handle_uploaded_file(request.FILES['file'])
            elif choice == 'DeviceAssignments':
                results = assignments_csv.handle_uploaded_file(request.FILES['file'])
            return render(request, 'ProcessManager/import.html', {'form': form, 'results': results})
    else:
        form = DataUploadForm()

    return render(request, 'ProcessManager/import.html', {'form': form})

def assignment_end_date(request, assignment_id):
    a = Assignment.objects.get(pk=assignment_id)
    if request.method == 'POST':
        form = AssignmentForm(request.POST)
        form.instance = a
        ##Allow for blank assignment to wipe things out.
        if request.POST.get('end_date') == "":
            a.end_date = None
            a.save()

        if form.is_valid():
            a.end_date = timezone.datetime.strptime(request.POST.get('end_date'), "%m/%d/%Y")
            a.save()

    return redirect('/person/%s/' % (a.person_id,))