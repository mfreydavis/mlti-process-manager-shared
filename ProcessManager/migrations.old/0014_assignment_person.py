# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0013_assignment'),
    ]

    operations = [
        migrations.AddField(
            model_name='assignment',
            name='person',
            field=models.ForeignKey(default='', to='ProcessManager.Person'),
            preserve_default=True,
        ),
    ]
