# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0016_assignment_asset'),
    ]

    operations = [
        migrations.AddField(
            model_name='assignment',
            name='start_date',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime.now),
            preserve_default=True,
        ),
    ]
