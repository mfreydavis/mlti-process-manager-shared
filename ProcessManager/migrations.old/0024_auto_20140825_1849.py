# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0023_remove_asset_assignment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assignment',
            name='asset',
            field=models.ForeignKey(default='', related_name='asset_assignment', to='ProcessManager.Asset'),
        ),
    ]
