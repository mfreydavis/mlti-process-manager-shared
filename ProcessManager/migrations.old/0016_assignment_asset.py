# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0015_assignment_note'),
    ]

    operations = [
        migrations.AddField(
            model_name='assignment',
            name='asset',
            field=models.ForeignKey(to='ProcessManager.Asset', default=''),
            preserve_default=True,
        ),
    ]
