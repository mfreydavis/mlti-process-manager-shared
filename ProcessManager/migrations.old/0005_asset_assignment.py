# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0004_asset_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='asset',
            name='assignment',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
