# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0010_person_first_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='birthdate',
            field=models.DateField(default='1990-01-01'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='person',
            name='middle_name',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='person',
            name='student_number',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
