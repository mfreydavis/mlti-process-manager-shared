# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0003_auto_20140818_1316'),
    ]

    operations = [
        migrations.AddField(
            model_name='asset',
            name='description',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
