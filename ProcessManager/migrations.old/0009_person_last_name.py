# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0008_person'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='last_name',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
