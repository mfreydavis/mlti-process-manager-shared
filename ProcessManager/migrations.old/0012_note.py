# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0011_auto_20140819_1452'),
    ]

    operations = [
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('content', models.TextField(verbose_name='')),
                ('person', models.ForeignKey(to='ProcessManager.Person')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
