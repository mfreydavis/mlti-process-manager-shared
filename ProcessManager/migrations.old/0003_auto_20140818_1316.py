# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0002_asset_tag'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asset',
            name='tag',
            field=models.IntegerField(),
        ),
    ]
