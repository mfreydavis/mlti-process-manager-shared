# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0017_assignment_start_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assignment',
            name='start_date',
            field=models.DateTimeField(default=django.utils.timezone.now, auto_now_add=True),
        ),
    ]
