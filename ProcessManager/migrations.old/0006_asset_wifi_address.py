# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0005_asset_assignment'),
    ]

    operations = [
        migrations.AddField(
            model_name='asset',
            name='wifi_address',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
