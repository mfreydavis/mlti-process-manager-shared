# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0018_auto_20140819_1837'),
    ]

    operations = [
        migrations.AddField(
            model_name='assignment',
            name='end_date',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
