# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0006_asset_wifi_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='asset',
            name='bluetooth_address',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
