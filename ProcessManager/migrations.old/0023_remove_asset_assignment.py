# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0022_asset_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='asset',
            name='assignment',
        ),
    ]
