# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0021_devicetype'),
    ]

    operations = [
        migrations.AddField(
            model_name='asset',
            name='type',
            field=models.ForeignKey(default=1, to='ProcessManager.DeviceType'),
            preserve_default=False,
        ),
    ]
