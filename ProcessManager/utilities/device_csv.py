__author__ = 'matt'

from ProcessManager.models import DeviceType
from django.core import exceptions
import csv


def handle_uploaded_file(file_in):
    path = 'ProcessManager/tmp/' + file_in._name
    ##Track Results
    updated = 0
    created = 0
    with open(path, 'wb') as dest:
        for chunk in file_in.chunks():
            dest.write(chunk)

    with open(path, 'rt') as in_csv:
        DeviceTypeCSV = csv.reader(in_csv)

        #Model Row:
        # Abed,Faris,MiddleName,130130220,7/22/98 00:00

        #Check to see if DeviceType exists
        ##Kill the header
        next(DeviceTypeCSV)

        for inDeviceType in DeviceTypeCSV:
            DeviceTypeName = inDeviceType[0]
            try:
                found_DeviceType = DeviceType.objects.get(name = inDeviceType[0])
                p = DeviceType(name=inDeviceType[0], description=inDeviceType[1])

                ##Write comparision for each field :( ##Tighten up and generalize this/move to object layer
                if (found_DeviceType.description != p.description):

                    ##Make sure its saves back as the correct object @TODO make this use the update function.
                    found_id = found_DeviceType.id
                    found_DeviceType = p
                    found_DeviceType.id = found_id
                    found_DeviceType.save()
                    updated += 1

            except DeviceType.DoesNotExist:
                    #If DeviceType does not exist, insert them.
                DeviceType.objects.create(name=inDeviceType[0], description=inDeviceType[1])
                created+=1
            except exceptions.MultipleObjectsReturned:
                pass

    return {'created' : created, 'updated' : updated}