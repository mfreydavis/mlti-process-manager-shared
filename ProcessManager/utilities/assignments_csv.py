__author__ = 'matt'


from ProcessManager.models import Asset, Person, Assignment
from django.utils import timezone
from django.core import exceptions
import csv

#@TODO Import Serial Numbers
def handle_uploaded_file(file_in):
    path = 'ProcessManager/tmp/' + file_in._name
    ##Track Results
    updated = 0
    created = 0
    failed = 0
    skipped =0
    with open(path, 'wb') as dest:
        for chunk in file_in.chunks():
            dest.write(chunk)

    with open(path, 'rt') as in_csv:
        assignmentCSV = csv.reader(in_csv)


        ##Kill the header
        next(assignmentCSV)

        for inAssignment in assignmentCSV:
            try:
                found_asset = Asset.objects.get(tag=inAssignment[1])
                found_person = Person.objects.get(student_number=inAssignment[0])


                ##Look for other assignments with that person & asset
                #End active assignments, and end future assignments on their start date.
                assignments = Assignment.objects.all().filter(asset=found_asset)
                for assignment in assignments:
                    if assignment.person == found_person and assignment.asset == found_asset and assignment.active is True:
                        skipped+=1
                    else:
                        if assignment.active is True:
                            ##Assignment ending in the future.
                            if assignment.end_date is None or assignment.end_date >= timezone.now():
                                assignment.end_date = timezone.now()
                                assignment.save()
                                updated+=1
                        elif assignment.start_date >= timezone.now() and (assignment.end_date is None or assignment.end_date >= timezone.now()):
                            assignment.end_date = assignment.start_date
                            assignment.save()
                            updated+=1


                an = Assignment.objects.create(person=found_person, asset=found_asset, start_date=timezone.now())
                created += 1






            except Person.DoesNotExist:
                failed+=1

            except Asset.DoesNotExist:
                failed+=1


    return {'created' : created, 'updated' : updated, 'failed': failed}
