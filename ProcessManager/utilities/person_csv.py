__author__ = 'matt'
from ProcessManager.models import Person
from django.core import exceptions

import csv

def emptyString(s):
    if s is '':
        return None
    else:
        return s

def handle_uploaded_file(file_in):
    path = 'ProcessManager/tmp/' + file_in._name
    ##Track Results
    updated = 0
    created = 0
    failed = 0

    with open(path, 'wb') as dest:
        for chunk in file_in.chunks():
            dest.write(chunk)

    with open(path, 'rt') as in_csv:
        personCSV = csv.reader(in_csv)

        #Model Row:
        # Abed,Faris,MiddleName,130130220,7/22/98 00:00

        #Check to see if person exists
        ##Kill the header
        next(personCSV)

        for inPerson in personCSV:
            person_id = inPerson[3]

            ##Catch if PersonID doesn't exist.
            if len(person_id) > 1:
                try:
                    found_person = Person.objects.get(student_number = person_id)
                    p = Person(last_name=inPerson[0], first_name=inPerson[1],middle_name=inPerson[2],
                              student_number=inPerson[3],  birthdate=emptyString(inPerson[4]))



                    ##Write comparision for each field :( ##Tighten up and generalize this/move to object layer
                    if (found_person.first_name != p.first_name or found_person.last_name != p.last_name
                        or found_person.middle_name != p.middle_name or found_person.birthdate != p.birthdate):

                        ##Make sure its saves back as the correct object @TODO make this use the update function.
                        found_id = found_person.id
                        found_person = p
                        found_person.id = found_id
                        found_person.save()
                        updated += 1

                except Person.DoesNotExist:
                        #If person does not exist, insert them.
                    try:
                            p = Person(last_name=inPerson[0], first_name=inPerson[1],middle_name=inPerson[2],
                                          student_number=inPerson[3], birthdate=emptyString(inPerson[4]) )

                            p.save()
                            created += 1

                    except exceptions.ValidationError:
                        failed += 1


                except exceptions.MultipleObjectsReturned:
                    failed += 1

                except exceptions.ValidationError:
                    failed += 1

            else:
                failed = failed + 1

    return {'created' : created, 'updated' : updated, 'failed' : failed}


