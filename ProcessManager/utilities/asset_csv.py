__author__ = 'matt'
from ProcessManager.models import Asset, DeviceType
from django.core import exceptions
import csv

#@TODO Import Serial Numbers
def handle_uploaded_file(file_in):
    path = 'ProcessManager/tmp/' + file_in._name
    ##Track Results
    updated = 0
    created = 0
    failed = 0
    with open(path, 'wb') as dest:
        for chunk in file_in.chunks():
            dest.write(chunk)

    with open(path, 'rt') as in_csv:
        assetCSV = csv.reader(in_csv)

        # Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
        # 2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,

        ##Kill the header
        next(assetCSV)

        for inAsset in assetCSV:
            try:
                found_asset = Asset.objects.get(tag=inAsset[0])

                d = DeviceType.objects.get_or_create(name=inAsset[1])[0]

                # p = d.asset_set.create(tag=inAsset[0], description=inAsset[2], wifi_address=inAsset[4],
                #        bluetooth_address=inAsset[5], passcode=inAsset[6])

                #Write comparision for each field :( ##Tighten up and generalize this/move to object layer
                if (found_asset.description != inAsset[2]
                    or found_asset.wifi_address != inAsset[4] or found_asset.bluetooth_address != inAsset[5]
                    or found_asset.passcode !=inAsset[5]):

                    p = Asset(tag=inAsset[0], description=inAsset[2], wifi_address=inAsset[4],
                       bluetooth_address=inAsset[5], passcode=inAsset[6])

                    ##Make sure its saves back as the correct object @TODO make this use the update function.
                    found_id = found_asset.id
                    found_type = found_asset.type
                    found_asset = p
                    found_asset.id = found_id
                    found_asset.type = found_type
                    found_asset.save()

                    if p.type != d:
                        p.type = d
                        p.save()
                        ##p.update(type=d)

                    updated += 1

            except Asset.DoesNotExist:
                #If Asset does not exist, insert them.
                d = DeviceType.objects.get_or_create(name=inAsset[1])[0]

                p = d.asset_set.create(tag=inAsset[0], description=inAsset[2], wifi_address=inAsset[4],
                       bluetooth_address=inAsset[5], passcode=inAsset[6])

                created+=1

            except exceptions.MultipleObjectsReturned:
                failed += 1



    return {'created' : created, 'updated' : updated, 'failed': failed}


