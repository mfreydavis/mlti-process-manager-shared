from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError
# Create your models here.

class DeviceType(models.Model):
    name = models.TextField(default='')
    description = models.TextField(default='')

    def __str__(self):
        return u'%s %s' % (self.name, self.description)


class Asset(models.Model):
    tag = models.IntegerField()
    description = models.TextField(default='')
    wifi_address = models.TextField(default='')
    bluetooth_address = models.TextField(default='')
    type = models.ForeignKey(DeviceType)
    passcode = models.TextField(default='')

    def __str__(self):
        return '{0}'.format(self.tag)

class Person(models.Model):
    last_name = models.TextField(default='')
    first_name = models.TextField(default='')
    middle_name = models.TextField(default='')
    student_number = models.TextField(default='')
    birthdate = models.DateField(default='1990-01-01',
                                 null=True)

    def __str__(self):
        return ('{last_name}, {first_name} : {student_number}').format(
            last_name=self.last_name, first_name=self.first_name, student_number=self.student_number)


class Note(models.Model):
    person = models.ForeignKey(Person)
    content = models.TextField('')
    json_details = models.TextField('')

    def __str__(self):
        return 'Note about {first_name} {last_name}'.format(first_name=self.first_name, last_name=self.last_name)


class Assignment(models.Model):
    person = models.ForeignKey(Person, default='')
    asset = models.ForeignKey(Asset, related_name='asset_assignment', default='')
    note = models.TextField(default='')
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(null=True)

    def clean(self):
        #Check that end date is after start date.
        if self.end_date is not None and self.end_date <= self.start_date:
            raise ValidationError('End date must be after start date.')
        else:
            pass

    def __str__(self):
        return '{asset} assigned to {first_name} {last_name} from {start_date} to {end_date}'.format(
            asset=self.asset.tag,
            first_name=self.person.first_name, last_name=self.person.last_name,
            start_date=self.start_date, end_date=self.end_date
        )

    @property
    def active(self):
        if (self.end_date is None or self.end_date >= timezone.now()) \
                and (self.start_date is None or self.start_date < timezone.now()):
            return True
        else:
            return False



class Accessory(models.Model):
    name = models.TextField(default='', null=False)
    asset = models.ForeignKey(Asset, null=True)

    def __str__(self):
        return '{name} for {asset}'.format(name=self.name, asset=self.asset.tag)


class CheckIn(models.Model):
    end_date = models.DateField(null=True)
    start_date = models.DateField(default=timezone.datetime.now().date(), null=False)
    note = models.TextField(null=True)

    def clean(self):
        #Check that end date is after start date.
        if self.end_date is not None and self.end_date <= self.start_date:
            raise ValidationError('End date must be after start date.')
        else:
            pass


    def __str__(self):
        return 'Checkin from {start_date} to {end_date}'.format(start_date=self.start_date, end_date=self.end_date)


class DeviceCheckIn(models.Model):
    MISSING = 'M'
    DAMAGED = 'D'
    PRESENT = 'P'
    STATUS_CHOICES = (
        (DAMAGED, 'Damaged'),
        (PRESENT, 'Present'),
        (MISSING, 'Missing'),
    )
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    assignment = models.ForeignKey(Assignment)
    check_in = models.ForeignKey(CheckIn)
    status = models.CharField(choices=STATUS_CHOICES, max_length=2)
    warranty = models.BooleanField(blank=True, default=False)


    class Meta:
        unique_together = (("assignment", "check_in"),)


    def __str__(self):
        return '{status} of {asset} for {check_in}. Last modified {last_modified}'.format(status=self.status,
                                                                                          asset=self.assignment.asset.tag,
                                                                                          check_in = self.check_in.id,
                                                                                          last_modified=self.last_modified
                                                                                          )


class AccessoryCheckIn(models.Model):
    MISSING = 'M'
    DAMAGED = 'D'
    PRESENT = 'P'
    STATUS_CHOICES = (
        (DAMAGED, 'Damaged'),
        (PRESENT, 'Present'),
        (MISSING, 'Missing'),
    )
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    assignment = models.ForeignKey(Assignment)
    accessory = models.ForeignKey(Accessory)
    check_in = models.ForeignKey(CheckIn)
    status = models.CharField(choices=STATUS_CHOICES, max_length=2, blank=True)
    warranty = models.BooleanField(blank=True, default=False)

    class Meta:
        unique_together = (("assignment", "accessory", "check_in"),)

    def __str__(self):
        return '{status} {accessory} of {asset} for {check_in}. Last modified {last_modified}'.format(status=self.status,
                                                                                                      accessory=self.accessory.name,
                                                                                          asset=self.assignment.asset.tag,
                                                                                          check_in = self.check_in.id,
                                                                                          last_modified=self.last_modified
                                                                                          )

class CheckInNote(models.Model):
    check_in = models.ForeignKey(CheckIn)
    assignment = models.ForeignKey(Assignment)
    content = models.TextField()

    unique_together = (("assignment", "check_in"),)

    def __str__(self):
        return self.content