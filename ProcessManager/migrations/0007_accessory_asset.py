# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0006_auto_20150416_1741'),
    ]

    operations = [
        migrations.AddField(
            model_name='accessory',
            name='asset',
            field=models.ForeignKey(null=True, to='ProcessManager.Asset'),
            preserve_default=True,
        ),
    ]
