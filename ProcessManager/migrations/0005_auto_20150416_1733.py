# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0004_accessory'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accessory',
            name='assignment',
            field=models.ForeignKey(to='ProcessManager.Asset', default=''),
        ),
    ]
