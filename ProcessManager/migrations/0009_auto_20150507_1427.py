# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0008_checkin'),
    ]

    operations = [
        migrations.AddField(
            model_name='checkin',
            name='end_date',
            field=models.DateField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='checkin',
            name='note',
            field=models.TextField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='checkin',
            name='start_date',
            field=models.DateField(default=datetime.datetime(2015, 5, 7, 14, 27, 53, 441922)),
            preserve_default=True,
        ),
    ]
