# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0013_auto_20150514_1322'),
    ]

    operations = [
        migrations.CreateModel(
            name='CheckInNote',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('content', models.TextField()),
                ('assignment', models.ForeignKey(to='ProcessManager.Assignment')),
                ('check_in', models.ForeignKey(to='ProcessManager.CheckIn')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
