# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Asset',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('tag', models.IntegerField()),
                ('description', models.TextField(default='')),
                ('wifi_address', models.TextField(default='')),
                ('bluetooth_address', models.TextField(default='')),
                ('passcode', models.TextField(default='')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Assignment',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('note', models.TextField(default='')),
                ('start_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('end_date', models.DateTimeField(null=True)),
                ('asset', models.ForeignKey(to='ProcessManager.Asset', related_name='asset_assignment', default='')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DeviceType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.TextField(default='')),
                ('description', models.TextField(default='')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('content', models.TextField(verbose_name='')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('last_name', models.TextField(default='')),
                ('first_name', models.TextField(default='')),
                ('middle_name', models.TextField(default='')),
                ('student_number', models.TextField(default='')),
                ('birthdate', models.DateField(default='1990-01-01')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='note',
            name='person',
            field=models.ForeignKey(to='ProcessManager.Person'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assignment',
            name='person',
            field=models.ForeignKey(to='ProcessManager.Person', default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='asset',
            name='type',
            field=models.ForeignKey(to='ProcessManager.DeviceType'),
            preserve_default=True,
        ),
    ]
