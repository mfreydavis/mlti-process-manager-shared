# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0005_auto_20150416_1733'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='accessory',
            name='assignment',
        ),
        migrations.AddField(
            model_name='accessory',
            name='name',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
