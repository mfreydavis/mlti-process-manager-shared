# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0010_auto_20150513_1937'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccessoryCheckIn',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(choices=[('D', 'Damaged'), ('P', 'Present'), ('M', 'Missing')], max_length=2)),
                ('accessory', models.ForeignKey(to='ProcessManager.Accessory')),
                ('assignment', models.ForeignKey(to='ProcessManager.Assignment')),
                ('check_in', models.ForeignKey(to='ProcessManager.CheckIn')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
