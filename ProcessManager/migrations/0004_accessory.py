# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0003_auto_20150410_1722'),
    ]

    operations = [
        migrations.CreateModel(
            name='Accessory',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('assignment', models.ForeignKey(default='', to='ProcessManager.Assignment')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
