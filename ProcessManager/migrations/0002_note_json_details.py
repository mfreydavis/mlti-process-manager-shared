# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='json_details',
            field=models.TextField(verbose_name='', default=''),
            preserve_default=False,
        ),
    ]
