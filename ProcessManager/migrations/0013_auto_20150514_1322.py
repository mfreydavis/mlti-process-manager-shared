# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0012_auto_20150514_1220'),
    ]

    operations = [
        migrations.AddField(
            model_name='devicecheckin',
            name='warranty',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='accessorycheckin',
            name='status',
            field=models.CharField(max_length=2, blank=True, choices=[('D', 'Damaged'), ('P', 'Present'), ('M', 'Missing')]),
        ),
        migrations.AlterField(
            model_name='accessorycheckin',
            name='warranty',
            field=models.BooleanField(default=False),
        ),
    ]
