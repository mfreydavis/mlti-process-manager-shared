# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0011_accessorycheckin'),
    ]

    operations = [
        migrations.AddField(
            model_name='accessorycheckin',
            name='warranty',
            field=models.BooleanField(default=None),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='checkin',
            name='start_date',
            field=models.DateField(default=datetime.date(2015, 5, 14)),
        ),
    ]
