# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0014_checkinnote'),
    ]

    operations = [
        migrations.AlterField(
            model_name='checkin',
            name='start_date',
            field=models.DateField(default=datetime.date(2015, 5, 19)),
        ),
        migrations.AlterUniqueTogether(
            name='accessorycheckin',
            unique_together=set([('assignment', 'accessory', 'check_in')]),
        ),
        migrations.AlterUniqueTogether(
            name='devicecheckin',
            unique_together=set([('assignment', 'check_in')]),
        ),
    ]
