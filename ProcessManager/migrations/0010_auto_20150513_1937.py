# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('ProcessManager', '0009_auto_20150507_1427'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeviceCheckIn',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(max_length=2, choices=[('D', 'Damaged'), ('P', 'Present'), ('M', 'Missing')])),
                ('assignment', models.ForeignKey(to='ProcessManager.Assignment')),
                ('check_in', models.ForeignKey(to='ProcessManager.CheckIn')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='checkin',
            name='start_date',
            field=models.DateField(default=datetime.date(2015, 5, 13)),
        ),
    ]
