__author__ = 'matt'

from django.test import TestCase

from ProcessManager.forms import AssignmentForm, CheckInForm, DeviceCheckInForm, AccessoryCheckInForm, CheckInNoteForm

class AssignmentFormTest(TestCase):

    def test_form_renders_item_text_input(self):

        form = AssignmentForm()
        self.assertIn('End date:', form.as_p())


class CheckInFormTest(TestCase):

    def test_form_renders_item_text(self):
        form = CheckInForm()
        self.assertIn('End date:', form.as_p())
        self.assertIn('Note:', form.as_p())
        self.assertIn('Start date:', form.as_p())


    def test_form_has_custom_css(self):
        form = CheckInForm()
        self.assertIn('class="form-control date_picker"', form.as_p())


class DeviceCheckInFormTest(TestCase):

    def test_form_renders_item_text(self):
        form = DeviceCheckInForm()
        self.assertIn('Status', form.as_p())

    def test_form_has_custom_css(self):
        form = CheckInForm()
        self.assertIn('class="form-control date_picker', form.as_p())


class AccessoryCheckInFormTest(TestCase):

    def test_form_renders_item_text(self):
        form = AccessoryCheckInForm()
        self.assertIn('status', form.as_p())
        self.assertIn('warranty', form.as_p())

    def test_form_has_custom_css(self):
        form = CheckInForm()
        self.assertIn('class="form-control date_picker', form.as_p())


class CheckInNoteFormTest(TestCase):

    def test_form_renders_item_text(self):
        form = CheckInNoteForm()
        self.assertIn('content', form.as_p())

