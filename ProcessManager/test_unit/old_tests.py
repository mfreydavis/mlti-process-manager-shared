from django.test import TestCase, Client
from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.db import transaction
from ProcessManager.utilities import person_csv
import csv
import datetime
from django.forms.models import model_to_dict

from ProcessManager.views import home_page
from ProcessManager.models import Asset, Person, Note, Assignment, DeviceType
from ProcessManager.forms import DataUploadForm


# Create your tests here.

class HomePagetest(TestCase):
    def test_root_url_resolves_to_home_page(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_uses_template(self):
        request = HttpRequest()
        response = home_page(request)
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'ProcessManager/home.html')

class SearchAssetTest(TestCase):
    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')


    def test_search_on_POST(self):
        test_asset = Asset.objects.create(tag=101, type=self.create_type_for_assignment())

        response = self.client.post(
            '/search/asset/',
            data={'asset_tag': 101}
        )

        found_item = Asset.objects.get(tag=101)

        self.assertEqual(found_item, test_asset)
        self.assertRedirects(response, '/asset/%d/' % (found_item.tag,))

    def test_search_results_when_not_asset_POST(self):
        response = self.client.post(
            '/search/asset/',
            data={'asset_tag': 12091}
        )

        found_item = Asset.objects.all().filter(tag=12091)
        self.assertEqual(found_item.count(), 0)
        self.assertRedirects(response, '/')

    def test_search_results_when_POST_is_not_digits(self):
        response = self.client.post(
            '/search/asset/',
            data={'asset_tag': 'this is a not it'}
        )

        self.assertRedirects(response, '/')


    def test_search_results_when_POST_is_empty(self):
        response = self.client.post(
            '/search/asset/',
            data={'asset_tag': ''}
        )

        self.assertRedirects(response, '/')


class SearchPersonTest(TestCase):
    def test_search_on_POST_by_last_name(self):
        test_person = Person.objects.create(id=1, last_name='Smith')

        response = self.client.post(
            '/search/person/',
            data={'last_name': 'Smith'}
        )

        found_person = Person.objects.get(last_name='Smith')

        self.assertEqual(found_person, test_person)
        self.assertRedirects(response, '/person/%d/' % (found_person.id,))

    def test_search_on_POST_by_student_number(self):
        test_person = Person.objects.create(id=1, student_number='10291')

        response = self.client.post(
            '/search/person/',
            data={'last_name': '10291'}
        )

        found_person = Person.objects.get(student_number='10291')

        self.assertEqual(found_person, test_person)
        self.assertRedirects(response, '/person/%d/' % (found_person.id,))


class SearchMultiplePersons(TestCase):
    def test_search_on_POST_request_and_return_multiple_matches(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')

        response = self.client.post(
            '/search/person/',
            data={'last_name': 'Person'}
        )

        found_persons = Person.objects.all().filter(last_name='Person')
        self.assertIn(test_person_1, found_persons)
        self.assertIn(test_person_2, found_persons)

        self.assertTemplateUsed('ProcessManager/select_person.html')


    def test_search_on_POST_and_return_multiple_matches_with_partial_match(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')

        response = self.client.post(
            '/search/person/',
            data={'last_name': 'rson'}
        )

        self.assertRedirects(response, '/select_person/rson/')
        self.assertTemplateUsed('ProcessManager/select_person.html')


    def test_search_on_POST_when_post_is_empty(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')

        response = self.client.post(
            '/search/person/',
            data={'last_name': ''}
        )

        self.assertRedirects(response, '/person/')
        self.assertTemplateUsed('ProcessManager/select_person.html')

    def test_search_on_POST_when_search_returns_no_results(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')

        response = self.client.post(
            '/search/person/',
            data={'last_name': 'nonsense'}
        )

        self.assertRedirects(response, '/select_person/nonsense/')
        self.assertTemplateUsed('ProcessManager/select_person.html')

    def test_search_on_POST_when_student_number_has_no_match(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='1209')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='12880')

        response = self.client.post(
            '/search/person/',
            data={'last_name': '121'}
        )

        self.assertRedirects(response, '/select_person/121/')
        self.assertTemplateUsed('ProcessManager/select_person.html')


class TestPersonDisplayPage(TestCase):
    def test_person_uses_correct_template(self):
        person = Person.objects.create(id=1)
        response = self.client.get('/person/%s/' % (person.id,))

        self.assertTemplateUsed(response, 'ProcessManager/person.html')

    def test_displays_details_from_that_person(self):
        correct_person = Person.objects.create(id=1,
                                               last_name='Frey-Davis',
                                               first_name='Matthew',
                                               middle_name='Scott',
                                               student_number='99999999',
                                               birthdate='1986-08-10',
        )
        response = self.client.get('/person/%s/' % (correct_person.id,))

        self.assertContains(response, 'Frey-Davis')
        self.assertContains(response, 'Matthew')
        self.assertContains(response, '99999999')
        # #Should figure out how to correctly test for the date.

    def test_passes_correct_list_to_template(self):
        correct_person = Person.objects.create(id=2)
        wrong_person = Person.objects.create(id=3)

        response = self.client.get('/person/%s/' % (correct_person.id,))

##@TODO marker
        self.assertEqual(response.context['person_information'], correct_person)


class TestPersonsDisplayPage(TestCase):
    def test_persons_uses_correct_template(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')

        people = Person.objects.all().filter(last_name__contains='Person')
        response = self.client.get('/select_person/%s/' % ('Person',))

        self.assertTemplateUsed(response, 'ProcessManager/select_person.html')

    def test_passes_correct_list_to_template(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='1',
                                              birthdate=timezone.now() - datetime.timedelta(weeks=520))
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='2',
                                              birthdate=timezone.now() - datetime.timedelta(days=30129))

        people = Person.objects.all().filter(last_name__contains='Person')
        response = self.client.get('/select_person/%s/' % ('Person',))

        self.assertEqual(response.context['persons_information'][0], people[0])
        self.assertEqual(response.context['persons_information'][1], people[1])
        self.assertCountEqual(response.context['persons_information'], people)


    def test_displays_from_the_correct_people(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='121314',
                                              birthdate='1990-01-01')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='238089',
                                              birthdate='2001-08-30')

        test_person_3 = Person.objects.create(last_name='James', first_name='Three', student_number='31231',
                                              birthdate='2002-09-29')
        test_person_4 = Person.objects.create(last_name='James', first_name='Four', student_number='412314',
                                              birthdate='2004-10-15')

        people = Person.objects.all().filter(last_name__contains='Person')
        response = self.client.get('/select_person/%s/' % ('Person',))

        self.assertContains(response, 'Person')
        self.assertContains(response, 'One')
        self.assertContains(response, '121314')
        self.assertContains(response, '01-01-1990')
        self.assertContains(response, 'Two')
        self.assertContains(response, '238089')
        self.assertContains(response, '08-30-2001')

        self.assertNotContains(response, 'James')
        self.assertNotContains(response, 'Three')
        self.assertNotContains(response, '31231')
        self.assertNotContains(response, '08-30-2002')

        self.assertNotContains(response, 'Four')
        self.assertNotContains(response, '412314')
        self.assertNotContains(response, '10-15-2004')


    def test_person_displays_if_it_has_preceeding_characters(self):
        test_person_1 = Person.objects.create(last_name='APerson', first_name='Flagship!')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')

        people = Person.objects.all().filter(last_name__contains='Person')
        response = self.client.get('/select_person/%s/' % ('Person',))

        self.assertContains(response, 'APerson')
        self.assertContains(response, 'Flagship!')

    def test_person_displays_if_they_have_postceeding_characters(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person-', first_name='Schooner')
        people = Person.objects.all().filter(last_name__contains='Person')
        response = self.client.get('/select_person/%s/' % ('Person',))

        self.assertContains(response, 'Person-')
        self.assertContains(response, 'Schooner')


    def test_person_displays_if_they_have_preceeding_and_postceeding_characters(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='APerson-', first_name='Flagship! Schooner')
        people = Person.objects.all().filter(last_name__contains='Person')
        response = self.client.get('/select_person/%s/' % ('Person',))

        self.assertContains(response, 'APerson-')
        self.assertContains(response, 'Flagship! Schooner')

    def test_person_displays_if_they_are_lacking_characters(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='APerson-', first_name='Flagship! Schooner')
        people = Person.objects.all().filter(last_name__contains='rson')

        response = self.client.get('/select_person/%s/' % ('rson',))

        self.assertContains(response, 'APerson-')
        self.assertContains(response, 'Flagship! Schooner')

    def test_persons_display_if_there_is_no_match_on_search(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='APerson-', first_name='Flagship! Schooner')
        people = Person.objects.all().filter(last_name__contains='rson')

        response = self.client.get('/select_person/%s/' % ('aslkdjalk',))

        self.assertContains(response, 'APerson-')
        self.assertContains(response, 'Flagship! Schooner')
        self.assertContains(response, 'Person')
        self.assertContains(response, 'One')


    # #THis may be a duplicate
    def test_persons_display_if_there_is_no_person(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='APerson-', first_name='Flagship! Schooner')
        people = Person.objects.all().filter(last_name__contains='rson')

        response = self.client.get('/person/')

        self.assertContains(response, 'APerson-')
        self.assertContains(response, 'Flagship! Schooner')
        self.assertContains(response, 'Person')
        self.assertContains(response, 'One')


class TestNoteDisplay(TestCase):
    def test_displays_note_from_correct_person(self):
        correct_person = Person.objects.create(id=2)
        wrong_person = Person.objects.create(id=3)
        correct_note = Note.objects.create(person=correct_person, content='Testing, Testing!')
        wrong_note = Note.objects.create(person=wrong_person, content='Danger, wrong note!')

        response = self.client.get('/person/%s/' % (correct_person.id,))

        self.assertContains(response, 'Testing, Testing!')
        self.assertNotContains(response, 'Danger, wrong note!')


class TestPersonModel(TestCase):
    def test_saving_and_retrieving_persons(self):
        first_person = Person(id=1,
                              last_name='Patten',
                              first_name='Tyler',
                              middle_name='Martin',
                              student_number='12345123',
                              birthdate='2014-09-01')
        first_person.save()

        saved_persons = Person.objects.all()

        self.assertEqual(saved_persons.count(), 1)

        first_saved_person = saved_persons[0]
        self.assertEqual(1, first_saved_person.id)
        self.assertEqual('Patten', first_saved_person.last_name)
        self.assertEqual('Tyler', first_saved_person.first_name)
        self.assertEqual('Martin', first_saved_person.middle_name)
        self.assertEqual('12345123', first_saved_person.student_number)
        self.assertEqual('2014-09-01', str(first_saved_person.birthdate))


class TestAssetDisplayPage(TestCase):
    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_uses_asset_template(self):
        asset_ = Asset.objects.create(tag=101, type=self.create_type_for_assignment())
        response = self.client.get('/asset/%d/' % (asset_.tag,))

        self.assertTemplateUsed(response, 'ProcessManager/asset.html')

    def test_passes_correct_list_to_template(self):
        correct_asset = Asset.objects.create(tag=101, type=self.create_type_for_assignment())
        wrong_asset = Asset.objects.create(tag=1091, type=self.create_type_for_assignment())

        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')
        right_assignment = Assignment.objects.create(asset=correct_asset, person=first_person,
                                                     start_date=timezone.now() - datetime.timedelta(days=2))
        wrong_assignment = Assignment.objects.create(asset=correct_asset, person=first_person,
                                                     start_date=timezone.now() - datetime.timedelta(days=10),
                                                     end_date=timezone.now() - datetime.timedelta(days=1))

        response = self.client.get('/asset/%d/' % (correct_asset.tag,))


        # #This doesn't feel quite right, though I think it is.  I feel like I'm cheating by putting it in a list.
        self.assertEqual(response.context['assignment_information'], [right_assignment])

    def test_passes_correct_assignments_to_template(self):
        correct_asset = Asset.objects.create(tag=101, type=self.create_type_for_assignment())
        wrong_asset = Asset.objects.create(tag=1091, type=self.create_type_for_assignment())

        response = self.client.get('/asset/%d/' % (correct_asset.tag,))

    def test_displays_details_from_that_asset(self):
        # #Person for asset to be assigned to
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        assignment = Assignment.objects.create(asset=correct_asset, person=first_person)
        response = self.client.get('/asset/%d/' % (correct_asset.tag,))

        self.assertContains(response, '101')
        self.assertContains(response, 'iPad')
        self.assertContains(response, 'Tyler M Patten')
        self.assertContains(response, '12:34:56:78:90'.upper())
        self.assertContains(response, 'ab:cd:ef:gh:hi'.upper())
        self.assertContains(response, '9876')


class TestUploadPersonCSV(TestCase):

    def test_uses_import_template(self):
        response = self.client.get('/import')

        self.assertTemplateUsed(response, 'ProcessManager/import.html')

#@TODO Bookmark!
class TestUploadAssignmentCSV(TestCase):

    def get_single_active_record(self, records):

        a = [x for x in records if x.active is True]
        if len(a) > 1:
            return self.fail("Too many active objects returned.")
        elif len(a) == 0:
            return self.fail("No active objects returned.")
        else:
            return a[0]


    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_new_assignment_is_created(self):

        ##Create a few devices
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        second_asset = Asset.objects.create(tag=3011836,
                                            description='Yes, Macbook',
                                            wifi_address='12:bb:cc:dd:12',
                                            bluetooth_address='12:bb:cc:dd:13',
                                            type=self.create_type_for_assignment(), )


        ##Create a few people
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='121314',
                                              birthdate='1990-01-01')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='238089',
                                              birthdate='2001-08-30')

        file_loc = '/Users/ICAdmin/PycharmProjects/mlti-manager/functional_tests/test_files/generated_files/u_assignments.csv'
        ##Assign them.
        with open(file_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
            ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
            tempCSV.writerow(['Student ID','Tag'])
            tempCSV.writerow(['121314', '3011835'])
            tempCSV.writerow(['238089', '3011836'])

        with open(file_loc, 'rb') as fcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceAssignments', 'file' : fcsv})



        first_assignment_asset = Assignment.objects.get(asset__tag = 3011835)
        second_assignment_asset = Assignment.objects.get(asset__tag = 3011836)
        first_assignment_person = Assignment.objects.get(person__student_number = 121314)
        second_assignment_person = Assignment.objects.get(person__student_number = 238089)

        self.assertEqual(first_assignment_asset.asset.tag, first_asset.tag )
        self.assertEqual(second_assignment_asset.asset.tag,  second_asset.tag)
        self.assertEqual(first_assignment_person.person.student_number,  test_person_1.student_number)
        self.assertEqual(second_assignment_person.person.student_number, test_person_2.student_number)

        correct_results = {'created' : 2, 'updated' : 0, 'failed' : 0}
        self.assertEquals(response.context['results'], correct_results)





    def test_end_date_updates_when_assignment_changes(self):
         ##Create a few devices


        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        second_asset = Asset.objects.create(tag=3011836,
                                            description='Yes, Macbook',
                                            wifi_address='12:bb:cc:dd:12',
                                            bluetooth_address='12:bb:cc:dd:13',
                                            type=self.create_type_for_assignment(), )


        ##Create a few people
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='121314',
                                              birthdate='1990-01-01')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='238089',
                                              birthdate='2001-08-30')

        test_person_3 = Person.objects.create(last_name='James', first_name='Three', student_number='31231',
                                              birthdate='2002-09-29')
        test_person_4 = Person.objects.create(last_name='James', first_name='Four', student_number='412314',
                                              birthdate='2004-10-15')

        pa1_id = Assignment.objects.create(person = test_person_3, asset=first_asset).id
        pa2_id = Assignment.objects.create(person = test_person_4, asset=second_asset).id


        file_loc = '/Users/ICAdmin/PycharmProjects/mlti-manager/functional_tests/test_files/generated_files/u_assignments.csv'
        with open(file_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
            ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
            tempCSV.writerow(['Student ID','Tag'])
            tempCSV.writerow(['121314', '3011835'])
            tempCSV.writerow(['238089', '3011836'])

        with open(file_loc, 'rb') as fcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceAssignments', 'file' : fcsv})


        ##Check for proper reassignment  && check active.
        first_assignment_asset = self.get_single_active_record(
            Assignment.objects.filter(asset__tag = 3011835))
        second_assignment_asset = self.get_single_active_record(
            Assignment.objects.filter(asset__tag = 3011836))

        ##Get prev assignment.
        prev_assignment_1 = Assignment.objects.get(id=pa1_id)
        prev_assignment_2 = Assignment.objects.get(id=pa2_id)

        self.assertEqual(prev_assignment_1.end_date.date(), timezone.now().date())
        self.assertEqual(prev_assignment_2.end_date.date(), timezone.now().date())


        correct_results = {'created' : 2, 'updated' : 2, 'failed' : 0}
        self.assertEquals(response.context['results'], correct_results)

    def test_start_date_set_when_assignment_created(self):

        ##Create a few devices
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        second_asset = Asset.objects.create(tag=3011836,
                                            description='Yes, Macbook',
                                            wifi_address='12:bb:cc:dd:12',
                                            bluetooth_address='12:bb:cc:dd:13',
                                            type=self.create_type_for_assignment(), )


        ##Create a few people
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='121314',
                                              birthdate='1990-01-01')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='238089',
                                              birthdate='2001-08-30')

        file_loc = '/Users/ICAdmin/PycharmProjects/mlti-manager/functional_tests/test_files/generated_files/u_assignments.csv'
        ##Assign them.
        with open(file_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
            ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
            tempCSV.writerow(['Student ID','Tag'])
            tempCSV.writerow(['121314', '3011835'])
            tempCSV.writerow(['238089', '3011836'])

        with open(file_loc, 'rb') as fcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceAssignments', 'file' : fcsv})

        ##Get prev assignment.
        prev_assignment_1 = Assignment.objects.get(asset=first_asset)
        prev_assignment_2 = Assignment.objects.get(asset=second_asset)

        self.assertEqual(prev_assignment_1.start_date.date(), timezone.now().date())
        self.assertEqual(prev_assignment_2.start_date.date(), timezone.now().date())


        correct_results = {'created' : 2, 'updated' : 0, 'failed' : 0}
        self.assertEquals(response.context['results'], correct_results)


    def test_dates_only_change_on_active_records_or_future_records(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        second_asset = Asset.objects.create(tag=3011836,
                                            description='Yes, Macbook',
                                            wifi_address='12:bb:cc:dd:12',
                                            bluetooth_address='12:bb:cc:dd:13',
                                            type=self.create_type_for_assignment(), )

        uninvolved_asset = Asset.objects.create(tag=1231091,
                                    description='Yes, Macbook',
                                    wifi_address='12:bb:cc:jd:12',
                                    bluetooth_address='12:bb:cc:jd:13',
                                    type=self.create_type_for_assignment(), )


        ##Create a few people
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='121314',
                                              birthdate='1990-01-01')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='238089',
                                              birthdate='2001-08-30')

        test_person_3 = Person.objects.create(last_name='James', first_name='Three', student_number='31231',
                                              birthdate='2002-09-29')
        test_person_4 = Person.objects.create(last_name='James', first_name='Four', student_number='412314',
                                              birthdate='2004-10-15')


        ##Create some to be changed.
        changed_assignment1 = Assignment.objects.create(person = test_person_3, asset=first_asset)
        changed_assignment2 = Assignment.objects.create(person = test_person_4, asset=second_asset)
        started_in_the_future = Assignment.objects.create(person=test_person_1, asset=first_asset,
                                                          start_date = timezone.now() + timezone.timedelta(days=10))

        ##Create some records not to be changed.
        ##With no start date but an end Date
        already_ended_assignment = Assignment.objects.create(person=test_person_3,
                                                             asset=first_asset,
                                                             end_date=timezone.now()- timezone.timedelta(days=1))

        ##With a start and an end date
        already_started_and_ended = Assignment.objects.create(person=test_person_3, asset=first_asset,
                                           start_date=timezone.now()-timezone.timedelta(days=3),
                                           end_date=timezone.now()- timezone.timedelta(days=1))

        ##An unrelated assignment.
        unrelated_assignment = Assignment.objects.create(person=test_person_3, asset=uninvolved_asset)


        file_loc = '/Users/ICAdmin/PycharmProjects/mlti-manager/functional_tests/test_files/generated_files/u_assignments.csv'
        with open(file_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
            ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
            tempCSV.writerow(['Student ID','Tag'])
            tempCSV.writerow(['121314', '3011835'])
            tempCSV.writerow(['238089', '3011836'])

        with open(file_loc, 'rb') as fcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceAssignments', 'file' : fcsv})

        ##Check to see the the correct items changed.
        self.assertNotEqual(model_to_dict(changed_assignment1),
                            model_to_dict(Assignment.objects.get(id=changed_assignment1.id)))
        self.assertNotEqual(model_to_dict(changed_assignment2),
                            model_to_dict(Assignment.objects.get(id=changed_assignment2.id)))

        ##Check future dated ones have changed.
        self.assertNotEqual(model_to_dict(started_in_the_future), model_to_dict(Assignment.objects.get(id=started_in_the_future.id)))
        self.assertNotEqual(started_in_the_future.end_date, Assignment.objects.get(id=started_in_the_future.id).start_date)

        ##Check for start and end date assignments haven't.
        self.assertEqual(model_to_dict(already_ended_assignment),
                         model_to_dict(Assignment.objects.get(id=already_ended_assignment.id)))
        self.assertEqual(model_to_dict(already_started_and_ended),
                         model_to_dict(Assignment.objects.get(id=already_started_and_ended.id)))

        ##Check the the unrelated asset is unchanged.
        self.assertEqual(model_to_dict(unrelated_assignment),
                         model_to_dict(Assignment.objects.get(id=unrelated_assignment.id)))


    def test_if_assignment_already_exists_do_nothing(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='121314',
                                              birthdate='1990-01-01')

        unchanged_assignment = Assignment.objects.create(person=test_person_1, asset=first_asset)


        file_loc = '/Users/ICAdmin/PycharmProjects/mlti-manager/functional_tests/test_files/generated_files/u_assignments.csv'
        with open(file_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
            ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
            tempCSV.writerow(['Student ID','Tag'])
            tempCSV.writerow(['121314', '3011835'])
            tempCSV.writerow(['238089', '3011836'])

        with open(file_loc, 'rb') as fcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceAssignments', 'file' : fcsv})

        self.assertEqual(model_to_dict(unchanged_assignment),
                         model_to_dict(Assignment.objects.get(id=unchanged_assignment.id)))



class TestUploadAssets(TestCase):

    def test_new_asset_is_created(self):

        ##Start Test.
        form = DataUploadForm()
        file_loc = 'functional_tests/test_files/Assets.csv'
        form.file = file_loc

        with open(file_loc, 'rb') as fcsv:
            self.client.post('/import', {'upload_type' : 'Assets', 'file' : fcsv})


        self.assertGreaterEqual(Asset.objects.all().count(), 100)

        found_device = Asset.objects.get(tag='2011634')


        ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,

        self.assertEqual("MacBook Air 11", found_device.type.name)
        self.assertEqual('84:38:35:57:E6:02', found_device.wifi_address)
        self.assertEqual('84:38:35:57:E6:03', found_device.bluetooth_address)
        self.assertEqual('', found_device.passcode)

    def test_asset_was_updated(self):

        #Start Test
        file_loc = 'functional_tests/test_files/generated_files/u_Assets.csv'

        form = DataUploadForm()
        form.file = file_loc
        ##Create a file to upload 2 types and change 1
        #Establish a device to change.
        d = DeviceType.objects.get_or_create(name='MacBook Air 11')[0]
        p = d.asset_set.create(tag='2011635', description='', wifi_address='85:38:35:57:E6:02',
                               bluetooth_address='85:38:35:57:E6:03', passcode='1092')

        with open(file_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
            ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
            tempCSV.writerow(['Tag','Type', 'Description', 'Serial Number', 'WiFi ID', 'Bluetooth ID', 'Passcode'])
            tempCSV.writerow(['2011634', 'MacBook Air 13','','C02L11UJFH51',
                              '84:38:35:57:E6:02','84:38:35:57:E6:03','0000'])
            tempCSV.writerow(['2011635', 'MacBook Air 11','','C02L11UJFH51',
                              '84:38:35:57:E6:12','84:38:35:57:E6:23','0020'])
            tempCSV.writerow(['2011636', 'MacBook Air 11','','C02L11UJFH51',
                              '84:38:35:57:E6:22','84:38:35:57:E6:33','0020'])

        with open(file_loc, 'rb') as fcsv:
            self.client.post('/import', {'upload_type' : 'Assets', 'file' : fcsv})


        correct_results = {'created' : 2, 'updated' : 1}
        #self.assertEquals(response.context.keys(), correct_results)


        self.assertEqual(Asset.objects.all().count(), 3)

        found_device = Asset.objects.get(tag='2011634')
        self.assertEquals('MacBook Air 13', found_device.type.name)
        self.assertEquals('84:38:35:57:E6:02', found_device.wifi_address)
        self.assertEquals('84:38:35:57:E6:03', found_device.bluetooth_address)
        self.assertEqual('0000', found_device.passcode)



class TestUploadDeviceTypeCSV(TestCase):

    ##No Format tests, because of refactor with the UploadPersonForm

    def test_new_device_type_is_created(self):
        form = DataUploadForm()
        file_loc = 'functional_tests/test_files/DeviceType.csv'
        form.file = file_loc

        with open(file_loc, 'rb') as fcsv:
            self.client.post('/import', {'upload_type' : 'DeviceType', 'file' : fcsv})

        self.assertGreaterEqual(DeviceType.objects.all().count(), 1)

        found_type = DeviceType.objects.get(name='MacBook Air 11')
        self.assertEquals('MacBook Air 11', found_type.name)
        self.assertEquals('MLTI Device', found_type.description)


    def test_device_type_was_updated(self):
        form = DataUploadForm()

        ##Create a file to upload 2 types and change 1
        #Establish a device to change.
        DeviceType.objects.create(name="MacBook Air 11", description="Early MacBook Laptop")

        test_loc = 'functional_tests/test_files/generated_files/DeviceType.csv'
        with open(test_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            tempCSV.writerow(['Name','Description'])
            tempCSV.writerow(['Acer c720p', 'Laptops purchased to and given to the science department.'])
            tempCSV.writerow(['MacBook Air 11', 'Tiny MacBook for Student Teachers'])
            tempCSV.writerow(['MacBook Air 13', 'The Normal MacBook'])


        with open(test_loc, 'rb') as pcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceType' ,'file' : pcsv})

        correct_results = {'created' : 2, 'updated' : 1}
        self.assertEquals(response.context['results'], correct_results)

        found_person = DeviceType.objects.get(name='MacBook Air 11')

        self.assertGreaterEqual(DeviceType.objects.all().count(), 3)

        found_type = DeviceType.objects.get(name='MacBook Air 11')
        self.assertEquals('MacBook Air 11', found_type.name)
        self.assertEquals('Tiny MacBook for Student Teachers', found_type.description)


    def test_correct_number_of_changes_was_recorded(self):
        form = DataUploadForm()

        ##Create a file to upload 2 types and change 1
        #Establish a device to change.
        DeviceType.objects.create(name="MacBook Air 11", description="Early MacBook Laptop")

        test_loc = 'functional_tests/test_files/generated_files/DeviceType.csv'
        with open(test_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            tempCSV.writerow(['Name','Description'])
            tempCSV.writerow(['Acer c720p', 'Laptops purchased to and given to the science department.'])
            tempCSV.writerow(['MacBook Air 11', 'Tiny MacBook for Student Teachers'])
            tempCSV.writerow(['MacBook Air 13', 'The Normal MacBook'])


        with open(test_loc, 'rb') as pcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceType' ,'file' : pcsv})

        correct_results = {'created' : 2, 'updated' : 1}

        self.assertEquals(response.context['results'], correct_results)



class TestUploadPersonCSVForm(TestCase):

    def test_form_item_input_has_placeholder_and_css_classes(self):
        form = DataUploadForm()
        self.assertIn('placeholder="Upload a file."', form.as_p())
        self.assertIn('class="form-control input-lg"', form.as_p())

    # @TODO Use a better upload file/example.
    def test_new_person_is_created(self):
        form = DataUploadForm()
        form.file = 'functional_tests/test_files/Persons.csv'

        with open('functional_tests/test_files/Persons.csv', 'rb') as pcsv:
            self.client.post('/import', { 'upload_type' : 'Person', 'file' : pcsv})

        self.assertGreaterEqual(Person.objects.all().count(), 3)
        found_person = Person.objects.get(student_number='130130220')
        self.assertEquals(found_person.first_name, 'Faris')
        self.assertEquals(found_person.last_name, 'Abed')
        self.assertEquals(found_person.student_number, '130130220')



    def test_person_was_updated(self):
        form = DataUploadForm()
        form.file = 'functional_tests/test_files/Persons.csv'

        #Establish a person to change.
        Person.objects.create(first_name='z_Faris',
                              last_name='z_Abed',
                              student_number='130130220')

        with open('functional_tests/test_files/Persons.csv', 'rb') as pcsv:
            self.client.post('/import', { 'upload_type' : 'Person', 'file' : pcsv})

        found_person = Person.objects.get(student_number='130130220')

        self.assertEquals(found_person.first_name, 'Faris')
        self.assertEquals(found_person.last_name, 'Abed')
        self.assertEquals(found_person.student_number, '130130220')

    def test_person_correct_number_of_changes_recorded(self):
        form = DataUploadForm()

        #Establish a person to change.
        Person.objects.create(first_name='z_Abed',
                              last_name='z_Farris',
                              student_number='130130220')

        ##Create a file to upload 2 people and change 1
        with open('functional_tests/test_files/generated_files/Persons.csv', 'w') as inFile:
            tempCSV = csv.writer(inFile)
            # Abed,Faris,MiddleName,130130220,7/22/98 00:00
            tempCSV.writerow(['Last Name','First Name', 'Middle Name', 'student number', 'dob'])
            tempCSV.writerow(['Abed', 'Faris', 'M', '130130220','1992-02-01'])
            tempCSV.writerow(['Morgan', 'Rachel', 'M', '130130210','1999-09-01'])
            tempCSV.writerow(['Binks', 'JarJar', 'M', '130130221','1994-10-02'])

        with open('functional_tests/test_files/generated_files/Persons.csv', 'rb') as pcsv:
            response = self.client.post('/import', {'upload_type' : 'Person' ,'file' : pcsv})

        correct_results = {'created' : 2, 'updated' : 1, 'failed' : 0}

        found_person = Person.objects.get(student_number='130130220')

        self.assertEquals('Faris',found_person.first_name)
        self.assertEquals('Abed', found_person.last_name )
        self.assertEquals('130130220', found_person.student_number)

        self.assertEquals(response.context['results'], correct_results)


    def test_failure_message_is_returned(self):
        form = DataUploadForm()

        #Establish a person to change.
        Person.objects.create(first_name='z_Abed',
                              last_name='z_Farris',
                              student_number='130130220')

        ##Create a file to upload 2 people and change 1
        with open('functional_tests/test_files/generated_files/u_invalid_Persons.csv', 'w') as inFile:
            tempCSV = csv.writer(inFile)
            # Abed,Faris,MiddleName,130130220,7/22/98 00:00
            tempCSV.writerow(['Last Name','First Name', 'Middle Name', 'student number', 'dob'])
            tempCSV.writerow(['Abed', '', 'M', '130130220','1992-02-01'])
            tempCSV.writerow(['', '', '', '',''])
            tempCSV.writerow(['Binks', '', 'M', '130130221','1994-10-02'])

        with open('functional_tests/test_files/generated_files/u_invalid_Persons.csv', 'rb') as pcsv:
            response = self.client.post('/import', {'upload_type' : 'Person' ,'file' : pcsv})

        correct_results = {'created' : 1, 'updated' : 1, 'failed' : 1}

        found_person = Person.objects.get(student_number='130130220')

        self.assertEquals('',found_person.first_name)
        self.assertEquals('Abed', found_person.last_name )
        self.assertEquals('130130220', found_person.student_number)

        self.assertEquals(response.context['results'], correct_results)

    def test_date_validation_failure_returns_error(self):
        form = DataUploadForm()
        ##Doesn't account for malformed dates -- yet.
        #Establish a person to change.
        Person.objects.create(first_name='z_Abed',
                              last_name='z_Farris',
                              student_number='130130220')

        ##Create a file to upload 2 people and change 1
        with open('functional_tests/test_files/generated_files/u_invalid_Persons.csv', 'w') as inFile:
            tempCSV = csv.writer(inFile)
            tempCSV.writerow(['Last Name','First Name', 'Middle Name', 'student number', 'dob'])
            tempCSV.writerows([['Abed','','','130130220','1998-07-22'],
            ['Abell','Lynda','','119521577','1999-01-04'],
            ['','Alexander','','109627607asdaa',''],
            ['Adams','Emily','','127420020','2001-11-18'],
            ['','','','',''],
            ['Albertson','Kelsea','','126066863','']
             ]
             )


        with open('functional_tests/test_files/generated_files/u_invalid_Persons.csv', 'rb') as pcsv:
            response = self.client.post('/import', {'upload_type' : 'Person' ,'file' : pcsv})

        correct_results = {'created' : 4, 'updated' : 1, 'failed' : 1}
        with transaction.atomic():
            found_person = Person.objects.get(student_number='130130220')

        self.assertEquals('',found_person.first_name)
        self.assertEquals('Abed', found_person.last_name )
        self.assertEquals('130130220', found_person.student_number)

        self.assertEquals(response.context['results'], correct_results)

class TestAssetModel(TestCase):
    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_saving_and_retrieving_assets(self):
        first_asset = Asset()
        first_asset.tag = 101
        first_asset.description = 'iPad'
        first_asset.wifi_address = '12:34:56:78:90'
        first_asset.bluetooth_address = 'ab:cd:ef:gh:hi'
        first_asset.type = self.create_type_for_assignment()
        first_asset.passcode = '8765'
        first_asset.save()

        second_asset = Asset()
        second_asset.tag = 102
        second_asset.description = 'Not iPad'
        second_asset.wifi_address = '12:19:90:90:90'
        second_asset.bluetooth_address = 'ab:19:19:19:19'
        second_asset.type = self.create_type_for_assignment()
        second_asset.save()

        saved_assets = Asset.objects.all()
        self.assertEqual(saved_assets.count(), 2)

        first_saved_asset = saved_assets[0]
        second_saved_asset = saved_assets[1]

        self.assertEqual(first_saved_asset.tag, 101)
        self.assertEqual(first_saved_asset.description, 'iPad')
        self.assertEqual(first_saved_asset.wifi_address, '12:34:56:78:90')
        self.assertEqual(first_saved_asset.bluetooth_address, 'ab:cd:ef:gh:hi')
        self.assertEqual(first_saved_asset.passcode, '8765')
        self.assertEqual(second_asset.tag, 102)


class TestNoteModel(TestCase):
    def test_can_saving_and_retrieving_notes(self):
        person_ = Person.objects.create(id=1)
        first_note = Note.objects.create(person=person_, content='This is a note.')

        saved_notes = Note.objects.all()
        self.assertEqual(saved_notes.count(), 1)

        first_saved_note = saved_notes[0]
        self.assertEqual(first_saved_note.person, person_)
        self.assertEqual(first_saved_note.content, 'This is a note.')


class TestAssignmentHistoryDisplay(TestCase):
    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_displays_details_from_that_assignment_history(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        second_asset = Asset.objects.create(tag=3011836,
                                            description='Yes, Macbook',
                                            wifi_address='12:bb:cc:dd:12',
                                            bluetooth_address='12:bb:cc:dd:13',
                                            type=self.create_type_for_assignment(), )

        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date='2014-01-01',
                                                     end_date='2014-02-02',
                                                     note='This is a note.')

        second_assignment = Assignment.objects.create(person=first_person,
                                                      asset=second_asset,
                                                      start_date='2012-01-01',
                                                      end_date=None,
                                                      note='Quick notes.')

        response = self.client.get('/person/%s/' % (first_person.id,))

        self.assertContains(response, first_asset.tag)
        self.assertContains(response, '01-01-2014')
        self.assertContains(response, '02-02-2014')
        self.assertContains(response, second_asset.tag)
        self.assertContains(response, '01-01-2012')

    def test_that_asset_text_is_a_clickable_link(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date='2014-01-01',
                                                     end_date='2014-02-02',
                                                     note='This is a note.')

        response = self.client.get('/person/%s/' % (first_person.id,))

        self.assertInHTML('<a href="/asset/%s/"> %s  </a>' % (first_asset.tag, first_asset.tag), str(response.content))


class AssignmentModelTest(TestCase):
    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_can_save_and_retrieve_asset_assignments(self):
        # #Create Assets for assignment.

        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment())

        second_asset = Asset.objects.create(tag=3011836,
                                            description='Yes, Macbook',
                                            wifi_address='12:bb:cc:dd:12',
                                            bluetooth_address='12:bb:cc:dd:13',
                                            type=self.create_type_for_assignment())

        ##Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date='2014-01-01',
                                                     end_date='2014-02-02',
                                                     note='This is a note.')

        second_assignment = Assignment.objects.create(person=first_person,
                                                      asset=second_asset,
                                                      start_date='2012-01-01',
                                                      end_date=None,
                                                      note='Quick notes.')

        saved_assignments = Assignment.objects.all()
        self.assertEqual(saved_assignments.count(), 2)

        first_saved_assignment = saved_assignments[0]
        second_saved_assignment = saved_assignments[1]
        ##Minimal testing, planning on a refactor later.
        self.assertEqual(first_saved_assignment.person, first_person)
        self.assertEqual(first_saved_assignment.asset, first_asset)
        self.assertEqual(first_saved_assignment.note, 'This is a note.')

        self.assertEqual(second_saved_assignment.person, first_person)
        self.assertEqual(second_saved_assignment.asset, second_asset)


    def test_sets_active_if_end_date_not_yet_here(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment())


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() - datetime.timedelta(days=3),
                                                     end_date=timezone.now() + datetime.timedelta(days=2),
                                                     note='This is a note.')

        self.assertTrue(first_assignment.active)


    def test_sets_active_if_end_date_is_none(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment())


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() - datetime.timedelta(days=3),
                                                     note='This is a note.')

        self.assertTrue(first_assignment.active)

    def test_sets_active_if_start_date_is_none(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment())


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     end_date=timezone.now() + datetime.timedelta(days=2),
                                                     note='This is a note.')

        self.assertTrue(first_assignment.active)

    def test_does_not_set_active_if_start_date_is_not_here_yet(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() + datetime.timedelta(days=2),
                                                     end_date=timezone.now() + datetime.timedelta(days=5),
                                                     note='This is a note.')

        self.assertFalse(first_assignment.active, 'Start Date is: %s' % str(first_assignment.start_date))


    def test_does_not_set_active_if_end_date_is_past(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment()
        )


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01',
        )

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() - datetime.timedelta(days=10),
                                                     end_date=timezone.now() - datetime.timedelta(days=2),
                                                     note='This is a note.')

        self.assertFalse(first_assignment.active)


class TestDeviceTypeModel(TestCase):
    def test_can_save_and_retrieve_Device_types(self):
        # #Create a device type
        device_type = DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

        saved_device_types = DeviceType.objects.all()

        self.assertEqual(saved_device_types.count(), 1)
        self.assertEqual(saved_device_types[0].name, 'Macbook')
        self.assertEqual(saved_device_types[0].description, '2013 Macbook Air')

    def test_can_assign_and_retrieve_a_Device_by_type(self):
        # #Create a device type
        device_type = DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

        ##Create a device with that type
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=device_type,
        )

        saved_devices = Asset.objects.all().filter(type=device_type)

        self.assertEqual(saved_devices.count(), 1)
        self.assertEqual(saved_devices[0].tag, 3011835)
        self.assertEqual(saved_devices[0].type.name, 'Macbook')
        self.assertEqual(saved_devices[0].type.description, '2013 Macbook Air', )


