from django.test import TestCase
from django.utils import timezone
from django.db import transaction
import csv
from django.forms.models import model_to_dict

from ProcessManager.models import Asset, Person, Assignment, DeviceType
from ProcessManager.forms import DataUploadForm


# Create your tests here.
class TestUploadPersonCSV(TestCase):

    def test_uses_import_template(self):
        response = self.client.get('/import')

        self.assertTemplateUsed(response, 'ProcessManager/import.html')

class TestUploadAssignmentCSV(TestCase):

    def get_single_active_record(self, records):

        a = [x for x in records if x.active is True]
        if len(a) > 1:
            return self.fail("Too many active objects returned.")
        elif len(a) == 0:
            return self.fail("No active objects returned.")
        else:
            return a[0]


    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_new_assignment_is_created(self):

        ##Create a few devices
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        second_asset = Asset.objects.create(tag=3011836,
                                            description='Yes, Macbook',
                                            wifi_address='12:bb:cc:dd:12',
                                            bluetooth_address='12:bb:cc:dd:13',
                                            type=self.create_type_for_assignment(), )


        ##Create a few people
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='121314',
                                              birthdate='1990-01-01')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='238089',
                                              birthdate='2001-08-30')

        file_loc = '/Users/ICAdmin/PycharmProjects/mlti-manager/functional_tests/test_files/generated_files/u_assignments.csv'
        ##Assign them.
        with open(file_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
            ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
            tempCSV.writerow(['Student ID','Tag'])
            tempCSV.writerow(['121314', '3011835'])
            tempCSV.writerow(['238089', '3011836'])

        with open(file_loc, 'rb') as fcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceAssignments', 'file' : fcsv})



        first_assignment_asset = Assignment.objects.get(asset__tag = 3011835)
        second_assignment_asset = Assignment.objects.get(asset__tag = 3011836)
        first_assignment_person = Assignment.objects.get(person__student_number = 121314)
        second_assignment_person = Assignment.objects.get(person__student_number = 238089)

        self.assertEqual(first_assignment_asset.asset.tag, first_asset.tag )
        self.assertEqual(second_assignment_asset.asset.tag,  second_asset.tag)
        self.assertEqual(first_assignment_person.person.student_number,  test_person_1.student_number)
        self.assertEqual(second_assignment_person.person.student_number, test_person_2.student_number)

        correct_results = {'created' : 2, 'updated' : 0, 'failed' : 0}
        self.assertEquals(response.context['results'], correct_results)





    def test_end_date_updates_when_assignment_changes(self):
         ##Create a few devices


        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        second_asset = Asset.objects.create(tag=3011836,
                                            description='Yes, Macbook',
                                            wifi_address='12:bb:cc:dd:12',
                                            bluetooth_address='12:bb:cc:dd:13',
                                            type=self.create_type_for_assignment(), )


        ##Create a few people
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='121314',
                                              birthdate='1990-01-01')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='238089',
                                              birthdate='2001-08-30')

        test_person_3 = Person.objects.create(last_name='James', first_name='Three', student_number='31231',
                                              birthdate='2002-09-29')
        test_person_4 = Person.objects.create(last_name='James', first_name='Four', student_number='412314',
                                              birthdate='2004-10-15')

        pa1_id = Assignment.objects.create(person = test_person_3, asset=first_asset).id
        pa2_id = Assignment.objects.create(person = test_person_4, asset=second_asset).id

        file_loc = '/Users/ICAdmin/PycharmProjects/mlti-manager/functional_tests/test_files/generated_files/u_assignments.csv'
        with open(file_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
            ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
            tempCSV.writerow(['Student ID','Tag'])
            tempCSV.writerow(['121314', '3011835'])
            tempCSV.writerow(['238089', '3011836'])

        with open(file_loc, 'rb') as fcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceAssignments', 'file' : fcsv})


        ##Check for proper reassignment  && check active.
        first_assignment_asset = self.get_single_active_record(
            Assignment.objects.filter(asset__tag = 3011835))
        second_assignment_asset = self.get_single_active_record(
            Assignment.objects.filter(asset__tag = 3011836))

        ##Get prev assignment.
        prev_assignment_1 = Assignment.objects.get(id=pa1_id)
        prev_assignment_2 = Assignment.objects.get(id=pa2_id)

        self.assertEqual(prev_assignment_1.end_date.date(), timezone.now().date())
        self.assertEqual(prev_assignment_2.end_date.date(), timezone.now().date())


        correct_results = {'created' : 2, 'updated' : 2, 'failed' : 0}
        self.assertEquals(response.context['results'], correct_results)

    def test_start_date_set_when_assignment_created(self):

        ##Create a few devices
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        second_asset = Asset.objects.create(tag=3011836,
                                            description='Yes, Macbook',
                                            wifi_address='12:bb:cc:dd:12',
                                            bluetooth_address='12:bb:cc:dd:13',
                                            type=self.create_type_for_assignment(), )


        ##Create a few people
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='121314',
                                              birthdate='1990-01-01')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='238089',
                                              birthdate='2001-08-30')

        file_loc = '/Users/ICAdmin/PycharmProjects/mlti-manager/functional_tests/test_files/generated_files/u_assignments.csv'
        ##Assign them.
        with open(file_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
            ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
            tempCSV.writerow(['Student ID','Tag'])
            tempCSV.writerow(['121314', '3011835'])
            tempCSV.writerow(['238089', '3011836'])

        with open(file_loc, 'rb') as fcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceAssignments', 'file' : fcsv})

        ##Get prev assignment.
        prev_assignment_1 = Assignment.objects.get(asset=first_asset)
        prev_assignment_2 = Assignment.objects.get(asset=second_asset)

        self.assertEqual(prev_assignment_1.start_date.date(), timezone.now().date())
        self.assertEqual(prev_assignment_2.start_date.date(), timezone.now().date())


        correct_results = {'created' : 2, 'updated' : 0, 'failed' : 0}
        self.assertEquals(response.context['results'], correct_results)


    def test_dates_only_change_on_active_records_or_future_records(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        second_asset = Asset.objects.create(tag=3011836,
                                            description='Yes, Macbook',
                                            wifi_address='12:bb:cc:dd:12',
                                            bluetooth_address='12:bb:cc:dd:13',
                                            type=self.create_type_for_assignment(), )

        uninvolved_asset = Asset.objects.create(tag=1231091,
                                    description='Yes, Macbook',
                                    wifi_address='12:bb:cc:jd:12',
                                    bluetooth_address='12:bb:cc:jd:13',
                                    type=self.create_type_for_assignment(), )


        ##Create a few people
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='121314',
                                              birthdate='1990-01-01')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='238089',
                                              birthdate='2001-08-30')

        test_person_3 = Person.objects.create(last_name='James', first_name='Three', student_number='31231',
                                              birthdate='2002-09-29')
        test_person_4 = Person.objects.create(last_name='James', first_name='Four', student_number='412314',
                                              birthdate='2004-10-15')


        ##Create some to be changed.
        changed_assignment1 = Assignment.objects.create(person = test_person_3, asset=first_asset)
        changed_assignment2 = Assignment.objects.create(person = test_person_4, asset=second_asset)
        started_in_the_future = Assignment.objects.create(person=test_person_1, asset=first_asset,
                                                          start_date = timezone.now() + timezone.timedelta(days=10))

        ##Create some records not to be changed.
        ##With no start date but an end Date
        already_ended_assignment = Assignment.objects.create(person=test_person_3,
                                                             asset=first_asset,
                                                             end_date=timezone.now()- timezone.timedelta(days=1))

        ##With a start and an end date
        already_started_and_ended = Assignment.objects.create(person=test_person_3, asset=first_asset,
                                           start_date=timezone.now()-timezone.timedelta(days=3),
                                           end_date=timezone.now()- timezone.timedelta(days=1))

        ##An unrelated assignment.
        unrelated_assignment = Assignment.objects.create(person=test_person_3, asset=uninvolved_asset)



        file_loc = '/Users/ICAdmin/PycharmProjects/mlti-manager/functional_tests/test_files/generated_files/u_assignments.csv'
        with open(file_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
            ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
            tempCSV.writerow(['Student ID','Tag'])
            tempCSV.writerow(['121314', '3011835'])
            tempCSV.writerow(['238089', '3011836'])

        with open(file_loc, 'rb') as fcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceAssignments', 'file' : fcsv})

        ##Check to see the the correct items changed.
        self.assertNotEqual(model_to_dict(changed_assignment1),
                            model_to_dict(Assignment.objects.get(id=changed_assignment1.id)))
        self.assertNotEqual(model_to_dict(changed_assignment2),
                            model_to_dict(Assignment.objects.get(id=changed_assignment2.id)))

        ##Check future dated ones have changed.
        self.assertNotEqual(model_to_dict(started_in_the_future), model_to_dict(Assignment.objects.get(id=started_in_the_future.id)))
        self.assertNotEqual(started_in_the_future.end_date, Assignment.objects.get(id=started_in_the_future.id).start_date)

        ##Check for start and end date assignments haven't.
        self.assertEqual(model_to_dict(already_ended_assignment),
                         model_to_dict(Assignment.objects.get(id=already_ended_assignment.id)))
        self.assertEqual(model_to_dict(already_started_and_ended),
                         model_to_dict(Assignment.objects.get(id=already_started_and_ended.id)))

        ##Check the the unrelated asset is unchanged.
        self.assertEqual(model_to_dict(unrelated_assignment),
                         model_to_dict(Assignment.objects.get(id=unrelated_assignment.id)))


    def test_if_assignment_already_exists_do_nothing(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='121314',
                                              birthdate='1990-01-01')

        unchanged_assignment = Assignment.objects.create(person=test_person_1, asset=first_asset)


        file_loc = '/Users/ICAdmin/PycharmProjects/mlti-manager/functional_tests/test_files/generated_files/u_assignments.csv'
        with open(file_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
            ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
            tempCSV.writerow(['Student ID','Tag'])
            tempCSV.writerow(['121314', '3011835'])
            tempCSV.writerow(['238089', '3011836'])

        with open(file_loc, 'rb') as fcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceAssignments', 'file' : fcsv})

        self.assertEqual(model_to_dict(unchanged_assignment),
                         model_to_dict(Assignment.objects.get(id=unchanged_assignment.id)))



class TestUploadAssets(TestCase):

    def test_new_asset_is_created(self):

        ##Start Test.
        form = DataUploadForm()
        file_loc = 'functional_tests/test_files/Assets.csv'
        form.file = file_loc

        with open(file_loc, 'rb') as fcsv:
            self.client.post('/import', {'upload_type' : 'Assets', 'file' : fcsv})


        self.assertGreaterEqual(Asset.objects.all().count(), 100)

        found_device = Asset.objects.get(tag='2011634')


        ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,

        self.assertEqual("MacBook Air 11", found_device.type.name)
        self.assertEqual('84:38:35:57:E6:02', found_device.wifi_address)
        self.assertEqual('84:38:35:57:E6:03', found_device.bluetooth_address)
        self.assertEqual('', found_device.passcode)

    def test_asset_was_updated(self):

        #Start Test
        file_loc = 'functional_tests/test_files/generated_files/u_Assets.csv'

        form = DataUploadForm()
        form.file = file_loc
        ##Create a file to upload 2 types and change 1
        #Establish a device to change.
        d = DeviceType.objects.get_or_create(name='MacBook Air 11')[0]
        p = d.asset_set.create(tag='2011635', description='', wifi_address='85:38:35:57:E6:02',
                               bluetooth_address='85:38:35:57:E6:03', passcode='1092')

        with open(file_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
            ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
            tempCSV.writerow(['Tag','Type', 'Description', 'Serial Number', 'WiFi ID', 'Bluetooth ID', 'Passcode'])
            tempCSV.writerow(['2011634', 'MacBook Air 13','','C02L11UJFH51',
                              '84:38:35:57:E6:02','84:38:35:57:E6:03','0000'])
            tempCSV.writerow(['2011635', 'MacBook Air 11','','C02L11UJFH51',
                              '84:38:35:57:E6:12','84:38:35:57:E6:23','0020'])
            tempCSV.writerow(['2011636', 'MacBook Air 11','','C02L11UJFH51',
                              '84:38:35:57:E6:22','84:38:35:57:E6:33','0020'])

        with open(file_loc, 'rb') as fcsv:
            self.client.post('/import', {'upload_type' : 'Assets', 'file' : fcsv})


        correct_results = {'created' : 2, 'updated' : 1}
        #self.assertEquals(response.context.keys(), correct_results)


        self.assertEqual(Asset.objects.all().count(), 3)

        found_device = Asset.objects.get(tag='2011634')
        self.assertEquals('MacBook Air 13', found_device.type.name)
        self.assertEquals('84:38:35:57:E6:02', found_device.wifi_address)
        self.assertEquals('84:38:35:57:E6:03', found_device.bluetooth_address)
        self.assertEqual('0000', found_device.passcode)



class TestUploadDeviceTypeCSV(TestCase):

    ##No Format tests, because of refactor with the UploadPersonForm

    def test_new_device_type_is_created(self):
        form = DataUploadForm()
        file_loc = 'functional_tests/test_files/DeviceType.csv'
        form.file = file_loc

        with open(file_loc, 'rb') as fcsv:
            self.client.post('/import', {'upload_type' : 'DeviceType', 'file' : fcsv})

        self.assertGreaterEqual(DeviceType.objects.all().count(), 1)

        found_type = DeviceType.objects.get(name='MacBook Air 11')
        self.assertEquals('MacBook Air 11', found_type.name)
        self.assertEquals('MLTI Device', found_type.description)


    def test_device_type_was_updated(self):
        form = DataUploadForm()

        ##Create a file to upload 2 types and change 1
        #Establish a device to change.
        DeviceType.objects.create(name="MacBook Air 11", description="Early MacBook Laptop")

        test_loc = 'functional_tests/test_files/generated_files/DeviceType.csv'
        with open(test_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            tempCSV.writerow(['Name','Description'])
            tempCSV.writerow(['Acer c720p', 'Laptops purchased to and given to the science department.'])
            tempCSV.writerow(['MacBook Air 11', 'Tiny MacBook for Student Teachers'])
            tempCSV.writerow(['MacBook Air 13', 'The Normal MacBook'])


        with open(test_loc, 'rb') as pcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceType' ,'file' : pcsv})

        correct_results = {'created' : 2, 'updated' : 1}
        self.assertEquals(response.context['results'], correct_results)

        found_person = DeviceType.objects.get(name='MacBook Air 11')

        self.assertGreaterEqual(DeviceType.objects.all().count(), 3)

        found_type = DeviceType.objects.get(name='MacBook Air 11')
        self.assertEquals('MacBook Air 11', found_type.name)
        self.assertEquals('Tiny MacBook for Student Teachers', found_type.description)


    def test_correct_number_of_changes_was_recorded(self):
        form = DataUploadForm()

        ##Create a file to upload 2 types and change 1
        #Establish a device to change.
        DeviceType.objects.create(name="MacBook Air 11", description="Early MacBook Laptop")

        test_loc = 'functional_tests/test_files/generated_files/DeviceType.csv'
        with open(test_loc, 'w') as inFile:
            tempCSV = csv.writer(inFile)
            tempCSV.writerow(['Name','Description'])
            tempCSV.writerow(['Acer c720p', 'Laptops purchased to and given to the science department.'])
            tempCSV.writerow(['MacBook Air 11', 'Tiny MacBook for Student Teachers'])
            tempCSV.writerow(['MacBook Air 13', 'The Normal MacBook'])


        with open(test_loc, 'rb') as pcsv:
            response = self.client.post('/import', {'upload_type' : 'DeviceType' ,'file' : pcsv})

        correct_results = {'created' : 2, 'updated' : 1}

        self.assertEquals(response.context['results'], correct_results)



class TestUploadPersonCSVForm(TestCase):

    def test_form_item_input_has_placeholder_and_css_classes(self):
        form = DataUploadForm()
        self.assertIn('placeholder="Upload a file."', form.as_p())
        self.assertIn('class="form-control input-lg"', form.as_p())

    # @TODO Use a better upload file/example.
    def test_new_person_is_created(self):
        form = DataUploadForm()
        form.file = 'functional_tests/test_files/Persons.csv'

        with open('functional_tests/test_files/Persons.csv', 'rb') as pcsv:
            self.client.post('/import', { 'upload_type' : 'Person', 'file' : pcsv})

        self.assertGreaterEqual(Person.objects.all().count(), 3)
        found_person = Person.objects.get(student_number='130130220')
        self.assertEquals(found_person.first_name, 'Faris')
        self.assertEquals(found_person.last_name, 'Abed')
        self.assertEquals(found_person.student_number, '130130220')



    def test_person_was_updated(self):
        form = DataUploadForm()
        form.file = 'functional_tests/test_files/Persons.csv'

        #Establish a person to change.
        Person.objects.create(first_name='z_Faris',
                              last_name='z_Abed',
                              student_number='130130220')

        with open('functional_tests/test_files/Persons.csv', 'rb') as pcsv:
            self.client.post('/import', { 'upload_type' : 'Person', 'file' : pcsv})

        found_person = Person.objects.get(student_number='130130220')

        self.assertEquals(found_person.first_name, 'Faris')
        self.assertEquals(found_person.last_name, 'Abed')
        self.assertEquals(found_person.student_number, '130130220')

    def test_person_correct_number_of_changes_recorded(self):
        form = DataUploadForm()

        #Establish a person to change.
        Person.objects.create(first_name='z_Abed',
                              last_name='z_Farris',
                              student_number='130130220')

        ##Create a file to upload 2 people and change 1
        with open('functional_tests/test_files/generated_files/Persons.csv', 'w') as inFile:
            tempCSV = csv.writer(inFile)
            # Abed,Faris,MiddleName,130130220,7/22/98 00:00
            tempCSV.writerow(['Last Name','First Name', 'Middle Name', 'student number', 'dob'])
            tempCSV.writerow(['Abed', 'Faris', 'M', '130130220','1992-02-01'])
            tempCSV.writerow(['Morgan', 'Rachel', 'M', '130130210','1999-09-01'])
            tempCSV.writerow(['Binks', 'JarJar', 'M', '130130221','1994-10-02'])

        with open('functional_tests/test_files/generated_files/Persons.csv', 'rb') as pcsv:
            response = self.client.post('/import', {'upload_type' : 'Person' ,'file' : pcsv})

        correct_results = {'created' : 2, 'updated' : 1, 'failed' : 0}

        found_person = Person.objects.get(student_number='130130220')

        self.assertEquals('Faris',found_person.first_name)
        self.assertEquals('Abed', found_person.last_name )
        self.assertEquals('130130220', found_person.student_number)

        self.assertEquals(response.context['results'], correct_results)


    def test_failure_message_is_returned(self):
        form = DataUploadForm()

        #Establish a person to change.
        Person.objects.create(first_name='z_Abed',
                              last_name='z_Farris',
                              student_number='130130220')

        ##Create a file to upload 2 people and change 1
        with open('functional_tests/test_files/generated_files/u_invalid_Persons.csv', 'w') as inFile:
            tempCSV = csv.writer(inFile)
            # Abed,Faris,MiddleName,130130220,7/22/98 00:00
            tempCSV.writerow(['Last Name','First Name', 'Middle Name', 'student number', 'dob'])
            tempCSV.writerow(['Abed', '', 'M', '130130220','1992-02-01'])
            tempCSV.writerow(['', '', '', '',''])
            tempCSV.writerow(['Binks', '', 'M', '130130221','1994-10-02'])

        with open('functional_tests/test_files/generated_files/u_invalid_Persons.csv', 'rb') as pcsv:
            response = self.client.post('/import', {'upload_type' : 'Person' ,'file' : pcsv})

        correct_results = {'created' : 1, 'updated' : 1, 'failed' : 1}

        found_person = Person.objects.get(student_number='130130220')

        self.assertEquals('',found_person.first_name)
        self.assertEquals('Abed', found_person.last_name )
        self.assertEquals('130130220', found_person.student_number)

        self.assertEquals(response.context['results'], correct_results)

    def test_date_validation_failure_returns_error(self):
        form = DataUploadForm()
        ##Doesn't account for malformed dates -- yet.
        #Establish a person to change.
        Person.objects.create(first_name='z_Abed',
                              last_name='z_Farris',
                              student_number='130130220')

        ##Create a file to upload 2 people and change 1
        with open('functional_tests/test_files/generated_files/u_invalid_Persons.csv', 'w') as inFile:
            tempCSV = csv.writer(inFile)
            tempCSV.writerow(['Last Name','First Name', 'Middle Name', 'student number', 'dob'])
            tempCSV.writerows([['Abed','','','130130220','1998-07-22'],
            ['Abell','Lynda','','119521577','1999-01-04'],
            ['','Alexander','','109627607asdaa',''],
            ['Adams','Emily','','127420020','2001-11-18'],
            ['','','','',''],
            ['Albertson','Kelsea','','126066863','']
             ]
             )


        with open('functional_tests/test_files/generated_files/u_invalid_Persons.csv', 'rb') as pcsv:
            response = self.client.post('/import', {'upload_type' : 'Person' ,'file' : pcsv})

        correct_results = {'created' : 4, 'updated' : 1, 'failed' : 1}
        with transaction.atomic():
            found_person = Person.objects.get(student_number='130130220')

        self.assertEquals('',found_person.first_name)
        self.assertEquals('Abed', found_person.last_name )
        self.assertEquals('130130220', found_person.student_number)

        self.assertEquals(response.context['results'], correct_results)
