from django.test import TestCase
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.db import IntegrityError
import datetime
from ProcessManager.models import Asset, Person, Note, Assignment, \
    DeviceType, Accessory, CheckIn, DeviceCheckIn, AccessoryCheckIn, CheckInNote


# Create your tests here.

class TestPersonModel(TestCase):
    def test_saving_and_retrieving_persons(self):
        first_person = Person(id=1,
                              last_name='Patten',
                              first_name='Tyler',
                              middle_name='Martin',
                              student_number='12345123',
                              birthdate='2014-09-01')
        first_person.save()

        saved_persons = Person.objects.all()

        self.assertEqual(saved_persons.count(), 1)

        first_saved_person = saved_persons[0]
        self.assertEqual(1, first_saved_person.id)
        self.assertEqual('Patten', first_saved_person.last_name)
        self.assertEqual('Tyler', first_saved_person.first_name)
        self.assertEqual('Martin', first_saved_person.middle_name)
        self.assertEqual('12345123', first_saved_person.student_number)
        self.assertEqual('2014-09-01', str(first_saved_person.birthdate))

class TestAssetModel(TestCase):
    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_saving_and_retrieving_assets(self):
        first_asset = Asset()
        first_asset.tag = 101
        first_asset.description = 'iPad'
        first_asset.wifi_address = '12:34:56:78:90'
        first_asset.bluetooth_address = 'ab:cd:ef:gh:hi'
        first_asset.type = self.create_type_for_assignment()
        first_asset.passcode = '8765'
        first_asset.save()

        second_asset = Asset()
        second_asset.tag = 102
        second_asset.description = 'Not iPad'
        second_asset.wifi_address = '12:19:90:90:90'
        second_asset.bluetooth_address = 'ab:19:19:19:19'
        second_asset.type = self.create_type_for_assignment()
        second_asset.save()

        saved_assets = Asset.objects.all()
        self.assertEqual(saved_assets.count(), 2)

        first_saved_asset = saved_assets[0]
        second_saved_asset = saved_assets[1]

        self.assertEqual(first_saved_asset.tag, 101)
        self.assertEqual(first_saved_asset.description, 'iPad')
        self.assertEqual(first_saved_asset.wifi_address, '12:34:56:78:90')
        self.assertEqual(first_saved_asset.bluetooth_address, 'ab:cd:ef:gh:hi')
        self.assertEqual(first_saved_asset.passcode, '8765')
        self.assertEqual(second_asset.tag, 102)


class TestNoteModel(TestCase):
    def test_can_saving_and_retrieving_notes(self):
        person_ = Person.objects.create(id=1)
        first_note = Note.objects.create(person=person_, content='This is a note.')

        saved_notes = Note.objects.all()
        self.assertEqual(saved_notes.count(), 1)

        first_saved_note = saved_notes[0]
        self.assertEqual(first_saved_note.person, person_)
        self.assertEqual(first_saved_note.content, 'This is a note.')

class AssignmentModelTest(TestCase):
    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_can_save_and_retrieve_asset_assignments(self):
        # #Create Assets for assignment.

        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment())

        second_asset = Asset.objects.create(tag=3011836,
                                            description='Yes, Macbook',
                                            wifi_address='12:bb:cc:dd:12',
                                            bluetooth_address='12:bb:cc:dd:13',
                                            type=self.create_type_for_assignment())

        ##Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date='2014-01-01',
                                                     end_date='2014-02-02',
                                                     note='This is a note.')

        second_assignment = Assignment.objects.create(person=first_person,
                                                      asset=second_asset,
                                                      start_date='2012-01-01',
                                                      end_date=None,
                                                      note='Quick notes.')

        saved_assignments = Assignment.objects.all()
        self.assertEqual(saved_assignments.count(), 2)

        first_saved_assignment = saved_assignments[0]
        second_saved_assignment = saved_assignments[1]
        ##Minimal testing, planning on a refactor later.
        self.assertEqual(first_saved_assignment.person, first_person)
        self.assertEqual(first_saved_assignment.asset, first_asset)
        self.assertEqual(first_saved_assignment.note, 'This is a note.')

        self.assertEqual(second_saved_assignment.person, first_person)
        self.assertEqual(second_saved_assignment.asset, second_asset)


    def test_sets_active_if_end_date_not_yet_here(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment())


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() - datetime.timedelta(days=3),
                                                     end_date=timezone.now() + datetime.timedelta(days=2),
                                                     note='This is a note.')

        self.assertTrue(first_assignment.active)


    def test_sets_active_if_end_date_is_none(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment())


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() - datetime.timedelta(days=3),
                                                     note='This is a note.')

        self.assertTrue(first_assignment.active)

    def test_sets_active_if_start_date_is_none(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment())


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     end_date=timezone.now() + datetime.timedelta(days=2),
                                                     note='This is a note.')

        self.assertTrue(first_assignment.active)

    def test_does_not_set_active_if_start_date_is_not_here_yet(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() + datetime.timedelta(days=2),
                                                     end_date=timezone.now() + datetime.timedelta(days=5),
                                                     note='This is a note.')

        self.assertFalse(first_assignment.active, 'Start Date is: %s' % str(first_assignment.start_date))


    def test_does_not_set_active_if_end_date_is_past(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment()
        )


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01',
        )

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() - datetime.timedelta(days=10),
                                                     end_date=timezone.now() - datetime.timedelta(days=2),
                                                     note='This is a note.')

        self.assertFalse(first_assignment.active)

    def test_that_validation_does_not_allow_end_date_before_start_dates(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment()
        )


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01',
        )

        first_assignment = Assignment(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() + datetime.timedelta(days=10),
                                                     end_date=timezone.now() - datetime.timedelta(days=10),
                                                     note='This is a note.')

        with self.assertRaises(ValidationError):
            first_assignment.save()
            first_assignment.full_clean()


class TestDeviceTypeModel(TestCase):
    def test_can_save_and_retrieve_Device_types(self):
        # #Create a device type
        device_type = DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

        saved_device_types = DeviceType.objects.all()

        self.assertEqual(saved_device_types.count(), 1)
        self.assertEqual(saved_device_types[0].name, 'Macbook')
        self.assertEqual(saved_device_types[0].description, '2013 Macbook Air')

    def test_can_assign_and_retrieve_a_Device_by_type(self):
        # #Create a device type
        device_type = DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

        ##Create a device with that type
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=device_type,
        )

        saved_devices = Asset.objects.all().filter(type=device_type)

        self.assertEqual(saved_devices.count(), 1)
        self.assertEqual(saved_devices[0].tag, 3011835)
        self.assertEqual(saved_devices[0].type.name, 'Macbook')
        self.assertEqual(saved_devices[0].type.description, '2013 Macbook Air', )


class TestAccessoryModel(TestCase):

    def test_can_save_and_retrieve_accessory_information(self):

        a = Accessory.objects.create(name="MacBook Air Charger Cord.  Shiny.")

        saved_accessories = Accessory.objects.all()
        self.assertEqual(saved_accessories.count(), 1)
        self.assertEqual(saved_accessories[0].name, 'MacBook Air Charger Cord.  Shiny.')


class TestCheckInNoteModel(TestCase):
    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_can_save_and_retrieve_notes(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment()
        )


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01',
        )

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() + datetime.timedelta(days=10),
                                                     end_date=timezone.now() - datetime.timedelta(days=10),
                                                     note='This is a note.')

        check = CheckIn.objects.create(start_date=timezone.now() - timezone.timedelta(days=5),
                            end_date=timezone.now() + timezone.timedelta(days=5),
                            note = 'This is a note.'
                          )


        a = CheckInNote.objects.create(content="This is a note that we saved.",
                                       check_in = check,
                                       assignment= first_assignment)
        saved_notes = CheckInNote.objects.all()
        self.assertEqual(saved_notes.count(), 1)
        self.assertEqual(saved_notes[0].content, 'This is a note that we saved.')

class TestCheckInModel(TestCase):

    def test_can_save_and_retrieve_check_in_information(self):

        c = CheckIn.objects.create(start_date=timezone.now() - timezone.timedelta(days=5),
                                    end_date=timezone.now() + timezone.timedelta(days=5),
                                    note = 'This is a note.'
                                  )
        all_check_in = CheckIn.objects.all()
        self.assertIn(c, all_check_in)

    def test_that_end_date_cannot_be_before_start_date(self):

        c = CheckIn(start_date=timezone.now() + timezone.timedelta(days=5),
                                    end_date=timezone.now() - timezone.timedelta(days=5),
                                    note='This is a note.'
                                  )

        with self.assertRaises(ValidationError):
            c.save()
            c.full_clean()


class TestDeviceCheckInModel(TestCase):

    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_can_save_and_retrieve_check_in_information(self):


        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment()
        )


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01',
        )

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() + datetime.timedelta(days=10),
                                                     end_date=timezone.now() - datetime.timedelta(days=10),
                                                     note='This is a note.')

        c = CheckIn.objects.create(start_date=timezone.now() - timezone.timedelta(days=5),
                            end_date=timezone.now() + timezone.timedelta(days=5),
                            note = 'This is a note.'
                          )

        device_check = DeviceCheckIn.objects.create(status=DeviceCheckIn.DAMAGED,
                          assignment=first_assignment,
                          check_in=c)

        saved_device_check_ins = DeviceCheckIn.objects.all()

        self.assertEqual(1, saved_device_check_ins.count())
        self.assertEqual(device_check, saved_device_check_ins[0])


    def test_last_modified_date_automatically_updates(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment()
        )


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01',
        )

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() + datetime.timedelta(days=10),
                                                     end_date=timezone.now() - datetime.timedelta(days=10),
                                                     note='This is a note.')

        c = CheckIn.objects.create(start_date=timezone.now() - timezone.timedelta(days=5),
                            end_date=timezone.now() + timezone.timedelta(days=5),
                            note = 'This is a note.'
                          )

        device_check = DeviceCheckIn.objects.create(status=DeviceCheckIn.MISSING,
                          assignment=first_assignment,
                          check_in=c)

        prev_last_modified = device_check.last_modified

        device_check.status = device_check.DAMAGED
        device_check.save()

        self.assertNotEqual(prev_last_modified, device_check.last_modified)

    def test_validation_fails_when_saving_duplicate_check_in(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment()
        )


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01',
        )

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() + datetime.timedelta(days=10),
                                                     end_date=timezone.now() - datetime.timedelta(days=10),
                                                     note='This is a note.')

        c = CheckIn.objects.create(start_date=timezone.now() - timezone.timedelta(days=5),
                            end_date=timezone.now() + timezone.timedelta(days=5),
                            note = 'This is a note.'
                          )

        device_check = DeviceCheckIn.objects.create(status=DeviceCheckIn.MISSING,
                          assignment=first_assignment,
                          check_in=c)

        duplicate_check = DeviceCheckIn(status=DeviceCheckIn.MISSING,
                          assignment=first_assignment,
                          check_in=c)

        with self.assertRaises(IntegrityError):
            duplicate_check.save()



class TestAccessoryCheckIn(TestCase):

    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_can_save_and_retrieve_check_in_information(self):


        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment()
        )


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01',
        )

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() + datetime.timedelta(days=10),
                                                     end_date=timezone.now() - datetime.timedelta(days=10),
                                                     note='This is a note.')

        c = CheckIn.objects.create(start_date=timezone.now() - timezone.timedelta(days=5),
                            end_date=timezone.now() + timezone.timedelta(days=5),
                            note = 'This is a note.'
                          )

        accessory = Accessory.objects.create(name='Sonic Screwdriver', asset=first_asset)

        accessory_check = AccessoryCheckIn.objects.create(status=AccessoryCheckIn.DAMAGED,
                          assignment=first_assignment,
                          check_in=c,
                          accessory=accessory)

        saved_accessory_check_ins = AccessoryCheckIn.objects.all()

        self.assertEqual(1, saved_accessory_check_ins.count())
        self.assertEqual(accessory_check, saved_accessory_check_ins[0])


    def test_last_modified_date_automatically_updates(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment()
        )


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01',
        )

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() + datetime.timedelta(days=10),
                                                     end_date=timezone.now() - datetime.timedelta(days=10),
                                                     note='This is a note.')

        c = CheckIn.objects.create(start_date=timezone.now() - timezone.timedelta(days=5),
                            end_date=timezone.now() + timezone.timedelta(days=5),
                            note = 'This is a note.'

                          )

        accessory = Accessory.objects.create(name='Sonic Screwdriver', asset=first_asset)

        accessory_check = AccessoryCheckIn.objects.create(status=AccessoryCheckIn.DAMAGED,
                          assignment=first_assignment,
                          check_in=c,
                          accessory=accessory)

        prev_last_modified = accessory_check.last_modified

        accessory_check.status = accessory_check.DAMAGED
        accessory_check.save()

        self.assertNotEqual(prev_last_modified, accessory_check.last_modified)


    def test_that_fields_must_be_unique_together(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment()
        )


        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01',
        )

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date=timezone.now() + datetime.timedelta(days=10),
                                                     end_date=timezone.now() - datetime.timedelta(days=10),
                                                     note='This is a note.')

        c = CheckIn.objects.create(start_date=timezone.now() - timezone.timedelta(days=5),
                            end_date=timezone.now() + timezone.timedelta(days=5),
                            note = 'This is a note.'

                          )

        accessory = Accessory.objects.create(name='Sonic Screwdriver', asset=first_asset)

        accessory_check = AccessoryCheckIn.objects.create(status=AccessoryCheckIn.DAMAGED,
                          assignment=first_assignment,
                          check_in=c,
                          accessory=accessory)

        duplicate_check = AccessoryCheckIn(status=AccessoryCheckIn.DAMAGED,
                          assignment=first_assignment,
                          check_in=c,
                          accessory=accessory)

        with self.assertRaises(IntegrityError):
            duplicate_check.save()




