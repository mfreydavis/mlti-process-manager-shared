__author__ = 'matt'

from django.test import TestCase
from django.utils import timezone

from django.forms.models import modelformset_factory


from ProcessManager.models import Asset, Person, Assignment, DeviceType, Accessory, CheckIn, AccessoryCheckIn
from ProcessManager.forms import DeviceCheckInForm, AccessoryCheckInForm, CheckInNoteForm


class CheckInSave(TestCase):
    def test_can_save_on_POST(self):
        end_date = timezone.now().date() + timezone.timedelta(days=5)
        start_date = timezone.now().date() - timezone.timedelta(days=1)
        note = 'A new check in.  For testing purposes :).'

        response = self.client.post('/checkin/new',
                                    data={'end_date': end_date,
                                          'start_date': start_date,
                                          'note': note}
                                    )
        found_check_in = CheckIn.objects.first()
        self.assertEqual(1, CheckIn.objects.all().count())

        self.assertEqual(found_check_in.note, note)
        self.assertEqual(found_check_in.end_date, end_date)
        self.assertEqual(found_check_in.start_date, start_date)

    def test_checkin_redirects_on_post(self):
        end_date = timezone.now().date() + timezone.timedelta(days=5)
        start_date = timezone.now().date() - timezone.timedelta(days=1)
        note = 'A new check in.  For testing purposes :).'

        response = self.client.post('/checkin/new',
                                    data={'end_date': end_date,
                                          'start_date': start_date,
                                          'note': note}
                                    )

        self.assertRedirects(response, '/')


class CheckInDeviceProcess(TestCase):
    def create_type_for_assignment(self):
        # Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def create_generic_check_in(self):
        return CheckIn.objects.create(end_date=timezone.now().date() + timezone.timedelta(days=5),
                                      start_date=timezone.now().date(),
                                      note='This is a note.')

    def test_display_uses_template(self):
        c = CheckIn.objects.create(end_date=timezone.now().date() + timezone.timedelta(days=5),
                                   start_date=timezone.now().date() - timezone.timedelta(days=1),
                                   note='A new check in.  For testing purposes :).')

        response = self.client.get('/checkin/{0}/'.format(c.id))

        self.assertTemplateUsed(response, 'ProcessManager/checkin.html')

    def test_display_page_displays_correct_person(self):
        c = self.create_generic_check_in()

        # Create Dummy Person
        correct_person = Person.objects.create(id=1,
                                               last_name='Smith',
                                               first_name='Sally',
                                               middle_name='Middle',
                                               student_number='12345678',
                                               birthdate='2014-08-18',

                                               )

        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        assignment = Assignment.objects.create(asset=correct_asset, person=correct_person)
        response = self.client.get('/checkin/{0}/{1}'.format(c.id, assignment.id))

        self.assertEqual(response.context['person_information'], correct_person)

    def test_display_page_displays_correct_asset(self):
        c = self.create_generic_check_in()
        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        # Create Dummy Person
        correct_person = Person.objects.create(id=1,
                                               last_name='Smith',
                                               first_name='Sally',
                                               middle_name='Middle',
                                               student_number='12345678',
                                               birthdate='2014-08-18',
                                               )
        assignment = Assignment.objects.create(asset=correct_asset, person=correct_person)
        response = self.client.get('/checkin/{0}/{1}'.format(c.id, assignment.id))

        self.assertEquals(correct_asset, response.context['assignment_information'].asset)

    def test_display_page_contains_context_for_accessories(self):
        c = self.create_generic_check_in()
        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        # Create Dummy Person
        correct_person = Person.objects.create(id=1,
                                               last_name='Smith',
                                               first_name='Sally',
                                               middle_name='Middle',
                                               student_number='12345678',
                                               birthdate='2014-08-18',
                                               )

        assignment = Assignment.objects.create(asset=correct_asset, person=correct_person)

        accessory1 = Accessory.objects.create(name='Sonic Screwdriver', asset=correct_asset)
        accessory2 = Accessory.objects.create(name='Tardis', asset=correct_asset)
        accessory3 = Accessory.objects.create(name='Anti-oil', asset=correct_asset)

        response = self.client.get('/checkin/{0}/{1}'.format(c.id, assignment.id))
        accessory_information = response.context['accessory_information']

        self.assertIn(accessory1, accessory_information)
        self.assertIn(accessory2, accessory_information)
        self.assertIn(accessory3, accessory_information)


    def test_check_in_page_uses_asset_form(self):
        c = self.create_generic_check_in()

        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        # Create Dummy Person
        correct_person = Person.objects.create(id=1,
                                               last_name='Smith',
                                               first_name='Sally',
                                               middle_name='Middle',
                                               student_number='12345678',
                                               birthdate='2014-08-18',
                                               )

        assignment = Assignment.objects.create(asset=correct_asset, person=correct_person)
        accessory1 = Accessory.objects.create(name='Sonic Screwdriver', asset=correct_asset)
        accessory2 = Accessory.objects.create(name='Tardis', asset=correct_asset)
        accessory3 = Accessory.objects.create(name='Anti-oil', asset=correct_asset)

        response = self.client.get('/checkin/{0}/{1}'.format(c.id, assignment.id))
        self.assertIsInstance(response.context['DeviceForms'], DeviceCheckInForm)

    def test_check_in_page_uses_accessory_formset(self):

        c = self.create_generic_check_in()

        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        # Create Dummy Person
        correct_person = Person.objects.create(id=1,
                                               last_name='Smith',
                                               first_name='Sally',
                                               middle_name='Middle',
                                               student_number='12345678',
                                               birthdate='2014-08-18',
                                               )

        assignment = Assignment.objects.create(asset=correct_asset, person=correct_person)
        accessory1 = Accessory.objects.create(name='Sonic Screwdriver', asset=correct_asset)
        accessory2 = Accessory.objects.create(name='Tardis', asset=correct_asset)
        accessory3 = Accessory.objects.create(name='Anti-oil', asset=correct_asset)

        response = self.client.get('/checkin/{0}/{1}'.format(c.id, assignment.id))

        # Work around that FormSets in Forms.py aren't working properly.
        self.assertEqual(str(type(response.context['AccessoryFormSet'])), "<class 'django.forms.formsets.AccessoryCheckInFormFormSet'>")


def test_page_uses_form_for_notes(self):
    c = self.create_generic_check_in()

    correct_asset = Asset.objects.create(tag=101,
                                         description='iPad',
                                         wifi_address='12:34:56:78:90',
                                         bluetooth_address='ab:cd:ef:gh:hi',
                                         type=self.create_type_for_assignment(),
                                         passcode='9876')

    # Create Dummy Person
    correct_person = Person.objects.create(id=1,
                                           last_name='Smith',
                                           first_name='Sally',
                                           middle_name='Middle',
                                           student_number='12345678',
                                           birthdate='2014-08-18',
                                           )

    assignment = Assignment.objects.create(asset=correct_asset, person=correct_person)


    response = self.client.get('/checkin/{0}/{1}'.format(c.id, assignment.id))
    self.assertIsInstance(response.context['NoteForm'], CheckInNoteForm)

    def test_check_in_saves_on_POST(self):
        self.fail('write the test')

    def test_check_in_redirects_on_POST(self):
        self.fail('write hte test')

# This needs to be refactored, since this is largely duplicative of the search view.
class CheckInPersonSearch(TestCase):
    def create_type_for_assignment(self):
        # Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def create_generic_check_in(self):
        return CheckIn.objects.create(end_date=timezone.now().date() + timezone.timedelta(days=5),
                                      start_date=timezone.now().date(),
                                      note='This is a note.')

    def test_search_on_POST_by_last_name(self):
        check_in = self.create_generic_check_in()
        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        # Create Dummy Person
        correct_person = Person.objects.create(id=1,
                                               last_name='Smith',
                                               first_name='Sally',
                                               middle_name='Middle',
                                               student_number='12345678',
                                               birthdate='2014-08-18',
                                               )
        assn = Assignment.objects.create(asset=correct_asset,
                                         person=correct_person,
                                         start_date=timezone.now().date(),
                                         end_date=timezone.now().date() + timezone.timedelta(days=5))

        response = self.client.post(
            '/search/checkin/{0}/person/'.format(check_in.id),
            data={'last_name': 'Smith'}
        )

        self.assertRedirects(response, '/checkin/{0}/{1}'.format(check_in.id, assn.id))

    def test_search_on_POST_by_student_number(self):
        test_person = Person.objects.create(id=1, student_number='10291')
        check_in = self.create_generic_check_in()
        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        assignment = Assignment.objects.create(asset=correct_asset,
                                               person=test_person,
                                               start_date=timezone.now().date(),
                                               end_date=timezone.now().date() + timezone.timedelta(days=5)
                                               )

        response = self.client.post(
            '/search/checkin/{0}/person/'.format(check_in.id),
            data={'last_name': '10291'}
        )

        self.assertRedirects(response, '/checkin/{0}/{1}'.format(check_in.id, assignment.id))


class SearchMultiplePersons(TestCase):
    def create_generic_check_in(self):
        return CheckIn.objects.create(end_date=timezone.now().date() + timezone.timedelta(days=5),
                                      start_date=timezone.now().date(),
                                      note='This is a note.')

    def test_search_on_POST_request_and_return_multiple_matches(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')
        check_in = self.create_generic_check_in()

        response = self.client.post(
            '/search/checkin/{0}/person/'.format(check_in.id),
            data={'last_name': 'Person'}
        )

        found_persons = Person.objects.all().filter(last_name='Person')
        self.assertIn(test_person_1, found_persons)
        self.assertIn(test_person_2, found_persons)

        self.assertTemplateUsed('ProcessManager/select_person.html')

    def test_search_on_POST_and_return_multiple_matches_with_partial_match(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')
        check_in = self.create_generic_check_in()

        response = self.client.post(
            '/search/checkin/{0}/person/'.format(check_in.id),
            data={'last_name': 'rson'}
        )

        self.assertRedirects(response, '/checkin/{0}/select_person/rson/'.format(check_in.id))
        self.assertTemplateUsed('ProcessManager/select_person.html')

    def test_search_on_POST_when_post_is_empty(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')
        check_in = self.create_generic_check_in()

        response = self.client.post(
            '/search/checkin/{0}/person/'.format(check_in.id),
            data={'last_name': ''}
        )

        self.assertRedirects(response, '/checkin/{0}/select_person/'.format(check_in.id))
        self.assertTemplateUsed('ProcessManager/select_person.html')

    def test_search_on_POST_when_search_returns_no_results(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')
        check_in = self.create_generic_check_in()

        response = self.client.post(
            '/search/checkin/{0}/person/'.format(check_in.id),
            data={'last_name': 'nonsense'}
        )

        self.assertRedirects(response, '/checkin/{0}/select_person/nonsense/'.format(check_in.id))
        self.assertTemplateUsed('ProcessManager/checkin_select_person.html')

    def test_search_on_POST_when_student_number_has_no_match(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='1209')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='12880')
        check_in = self.create_generic_check_in()

        response = self.client.post(
            '/search/checkin/{0}/person/'.format(check_in.id),
            data={'last_name': '121'}
        )

        self.assertRedirects(response, '/checkin/{0}/select_person/121/'.format(check_in.id))
        self.assertTemplateUsed('ProcessManager/select_person.html')
