from django.test import TestCase
from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.utils import timezone
import datetime

from ProcessManager.views import home_page
from ProcessManager.models import Asset, Person, Note, Assignment, DeviceType, Accessory, CheckIn
from ProcessManager.forms import AssignmentForm, CheckInForm

# Create your tests here.

class HomePagetest(TestCase):
    def test_root_url_resolves_to_home_page(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_uses_template(self):
        self.assertTemplateUsed('/')


    def test_home_page_include_checkin_form(self):

        response = self.client.get('/')

        self.assertIsInstance(response.context['form'], CheckInForm)

    def test_home_page_includes_existing_checkins(self):
        start_date = timezone.now() - timezone.timedelta(days=5)
        end_date = timezone.now() + timezone.timedelta(days=5)
        note = 'This is a note.'

        c = CheckIn.objects.create(start_date=start_date,
                            end_date=end_date,
                            note = note
                          )

        response = self.client.get('/')

        check = response.context['checkins']

        self.assertIn(c, check)


class SearchAssetTest(TestCase):
    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')


    def test_search_on_POST(self):
        test_asset = Asset.objects.create(tag=101, type=self.create_type_for_assignment())

        response = self.client.post(
            '/search/asset/',
            data={'asset_tag': 101}
        )

        found_item = Asset.objects.get(tag=101)

        self.assertEqual(found_item, test_asset)
        self.assertRedirects(response, '/asset/%d/' % (found_item.tag,))

    def test_search_results_when_not_asset_POST(self):
        response = self.client.post(
            '/search/asset/',
            data={'asset_tag': 12091}
        )

        found_item = Asset.objects.all().filter(tag=12091)
        self.assertEqual(found_item.count(), 0)
        self.assertRedirects(response, '/')

    def test_search_results_when_POST_is_not_digits(self):
        response = self.client.post(
            '/search/asset/',
            data={'asset_tag': 'this is a not it'}
        )

        self.assertRedirects(response, '/')


    def test_search_results_when_POST_is_empty(self):
        response = self.client.post(
            '/search/asset/',
            data={'asset_tag': ''}
        )

        self.assertRedirects(response, '/')


class SearchPersonTest(TestCase):
    def test_search_on_POST_by_last_name(self):
        test_person = Person.objects.create(id=1, last_name='Smith')

        response = self.client.post(
            '/search/person/',
            data={'last_name': 'Smith'}
        )

        found_person = Person.objects.get(last_name='Smith')

        self.assertEqual(found_person, test_person)
        self.assertRedirects(response, '/person/%d/' % (found_person.id,))

    def test_search_on_POST_by_student_number(self):
        test_person = Person.objects.create(id=1, student_number='10291')

        response = self.client.post(
            '/search/person/',
            data={'last_name': '10291'}
        )

        found_person = Person.objects.get(student_number='10291')

        self.assertEqual(found_person, test_person)
        self.assertRedirects(response, '/person/%d/' % (found_person.id,))


class SearchMultiplePersons(TestCase):
    def test_search_on_POST_request_and_return_multiple_matches(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')

        response = self.client.post(
            '/search/person/',
            data={'last_name': 'Person'}
        )

        found_persons = Person.objects.all().filter(last_name='Person')
        self.assertIn(test_person_1, found_persons)
        self.assertIn(test_person_2, found_persons)

        self.assertTemplateUsed('ProcessManager/select_person.html')


    def test_search_on_POST_and_return_multiple_matches_with_partial_match(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')

        response = self.client.post(
            '/search/person/',
            data={'last_name': 'rson'}
        )

        self.assertRedirects(response, '/select_person/rson/')
        self.assertTemplateUsed('ProcessManager/select_person.html')


    def test_search_on_POST_when_post_is_empty(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')

        response = self.client.post(
            '/search/person/',
            data={'last_name': ''}
        )

        self.assertRedirects(response, '/person/')
        self.assertTemplateUsed('ProcessManager/select_person.html')

    def test_search_on_POST_when_search_returns_no_results(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')

        response = self.client.post(
            '/search/person/',
            data={'last_name': 'nonsense'}
        )

        self.assertRedirects(response, '/select_person/nonsense/')
        self.assertTemplateUsed('ProcessManager/select_person.html')

    def test_search_on_POST_when_student_number_has_no_match(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='1209')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='12880')

        response = self.client.post(
            '/search/person/',
            data={'last_name': '121'}
        )

        self.assertRedirects(response, '/select_person/121/')
        self.assertTemplateUsed('ProcessManager/select_person.html')


class TestPersonDisplayPage(TestCase):
    def test_person_uses_correct_template(self):
        person = Person.objects.create(id=1)
        response = self.client.get('/person/%s/' % (person.id,))

        self.assertTemplateUsed(response, 'ProcessManager/person.html')

    def test_displays_details_from_that_person(self):
        correct_person = Person.objects.create(id=1,
                                               last_name='Frey-Davis',
                                               first_name='Matthew',
                                               middle_name='Scott',
                                               student_number='99999999',
                                               birthdate='1986-08-10',
        )
        response = self.client.get('/person/%s/' % (correct_person.id,))

        self.assertContains(response, 'Frey-Davis')
        self.assertContains(response, 'Matthew')
        self.assertContains(response, '99999999')
        # #Should figure out how to correctly test for the date.

    def test_passes_correct_list_to_template(self):
        correct_person = Person.objects.create(id=2)
        wrong_person = Person.objects.create(id=3)

        response = self.client.get('/person/%s/' % (correct_person.id,))

        self.assertEqual(response.context['person_information'], correct_person)


class TestPersonsDisplayPage(TestCase):
    def test_persons_uses_correct_template(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')

        people = Person.objects.all().filter(last_name__contains='Person')
        response = self.client.get('/select_person/%s/' % ('Person',))

        self.assertTemplateUsed(response, 'ProcessManager/select_person.html')

    def test_passes_correct_list_to_template(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='1',
                                              birthdate=timezone.now() - datetime.timedelta(weeks=520))
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='2',
                                              birthdate=timezone.now() - datetime.timedelta(days=30129))

        people = Person.objects.all().filter(last_name__contains='Person')
        response = self.client.get('/select_person/%s/' % ('Person',))

        self.assertEqual(response.context['persons_information'][0], people[0])
        self.assertEqual(response.context['persons_information'][1], people[1])
        self.assertCountEqual(response.context['persons_information'], people)


    def test_displays_from_the_correct_people(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One', student_number='121314',
                                              birthdate='1990-01-01')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two', student_number='238089',
                                              birthdate='2001-08-30')

        test_person_3 = Person.objects.create(last_name='James', first_name='Three', student_number='31231',
                                              birthdate='2002-09-29')
        test_person_4 = Person.objects.create(last_name='James', first_name='Four', student_number='412314',
                                              birthdate='2004-10-15')

        people = Person.objects.all().filter(last_name__contains='Person')
        response = self.client.get('/select_person/%s/' % ('Person',))

        self.assertContains(response, 'Person')
        self.assertContains(response, 'One')
        self.assertContains(response, '121314')
        self.assertContains(response, '01-01-1990')
        self.assertContains(response, 'Two')
        self.assertContains(response, '238089')
        self.assertContains(response, '08-30-2001')

        self.assertNotContains(response, 'James')
        self.assertNotContains(response, 'Three')
        self.assertNotContains(response, '31231')
        self.assertNotContains(response, '08-30-2002')

        self.assertNotContains(response, 'Four')
        self.assertNotContains(response, '412314')
        self.assertNotContains(response, '10-15-2004')


    def test_person_displays_if_it_has_preceeding_characters(self):
        test_person_1 = Person.objects.create(last_name='APerson', first_name='Flagship!')
        test_person_2 = Person.objects.create(last_name='Person', first_name='Two')

        people = Person.objects.all().filter(last_name__contains='Person')
        response = self.client.get('/select_person/%s/' % ('Person',))

        self.assertContains(response, 'APerson')
        self.assertContains(response, 'Flagship!')

    def test_person_displays_if_they_have_postceeding_characters(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='Person-', first_name='Schooner')
        people = Person.objects.all().filter(last_name__contains='Person')
        response = self.client.get('/select_person/%s/' % ('Person',))

        self.assertContains(response, 'Person-')
        self.assertContains(response, 'Schooner')


    def test_person_displays_if_they_have_preceeding_and_postceeding_characters(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='APerson-', first_name='Flagship! Schooner')
        people = Person.objects.all().filter(last_name__contains='Person')
        response = self.client.get('/select_person/%s/' % ('Person',))

        self.assertContains(response, 'APerson-')
        self.assertContains(response, 'Flagship! Schooner')

    def test_person_displays_if_they_are_lacking_characters(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='APerson-', first_name='Flagship! Schooner')
        people = Person.objects.all().filter(last_name__contains='rson')

        response = self.client.get('/select_person/%s/' % ('rson',))

        self.assertContains(response, 'APerson-')
        self.assertContains(response, 'Flagship! Schooner')

    def test_persons_display_if_there_is_no_match_on_search(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='APerson-', first_name='Flagship! Schooner')
        people = Person.objects.all().filter(last_name__contains='rson')

        response = self.client.get('/select_person/%s/' % ('aslkdjalk',))

        self.assertContains(response, 'APerson-')
        self.assertContains(response, 'Flagship! Schooner')
        self.assertContains(response, 'Person')
        self.assertContains(response, 'One')


    # #THis may be a duplicate
    def test_persons_display_if_there_is_no_person(self):
        test_person_1 = Person.objects.create(last_name='Person', first_name='One')
        test_person_2 = Person.objects.create(last_name='APerson-', first_name='Flagship! Schooner')
        people = Person.objects.all().filter(last_name__contains='rson')

        response = self.client.get('/person/')

        self.assertContains(response, 'APerson-')
        self.assertContains(response, 'Flagship! Schooner')
        self.assertContains(response, 'Person')
        self.assertContains(response, 'One')


class TestNoteDisplay(TestCase):
    def test_displays_note_from_correct_person(self):
        correct_person = Person.objects.create(id=2)
        wrong_person = Person.objects.create(id=3)
        correct_note = Note.objects.create(person=correct_person, content='Testing, Testing!')
        wrong_note = Note.objects.create(person=wrong_person, content='Danger, wrong note!')

        response = self.client.get('/person/%s/' % (correct_person.id,))

        self.assertContains(response, 'Testing, Testing!')
        self.assertNotContains(response, 'Danger, wrong note!')

class TestAssetDisplayPage(TestCase):
    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_uses_asset_template(self):
        asset_ = Asset.objects.create(tag=101, type=self.create_type_for_assignment())
        response = self.client.get('/asset/%d/' % (asset_.tag,))

        self.assertTemplateUsed(response, 'ProcessManager/asset.html')

    def test_passes_correct_list_to_template(self):
        correct_asset = Asset.objects.create(tag=101, type=self.create_type_for_assignment())
        wrong_asset = Asset.objects.create(tag=1091, type=self.create_type_for_assignment())

        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')
        right_assignment = Assignment.objects.create(asset=correct_asset, person=first_person,
                                                     start_date=timezone.now() - datetime.timedelta(days=2))
        wrong_assignment = Assignment.objects.create(asset=correct_asset, person=first_person,
                                                     start_date=timezone.now() - datetime.timedelta(days=10),
                                                     end_date=timezone.now() - datetime.timedelta(days=1))

        response = self.client.get('/asset/%d/' % (correct_asset.tag,))


        # #This doesn't feel quite right, though I think it is.  I feel like I'm cheating by putting it in a list.
        self.assertEqual(response.context['assignment_information'], [right_assignment])

    def test_passes_correct_assignments_to_template(self):
        correct_asset = Asset.objects.create(tag=101, type=self.create_type_for_assignment())
        wrong_asset = Asset.objects.create(tag=1091, type=self.create_type_for_assignment())

        response = self.client.get('/asset/%d/' % (correct_asset.tag,))

    def test_displays_details_from_that_asset(self):
        # #Person for asset to be assigned to
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        assignment = Assignment.objects.create(asset=correct_asset, person=first_person)
        response = self.client.get('/asset/%d/' % (correct_asset.tag,))

        self.assertContains(response, '101')
        self.assertContains(response, 'iPad')
        self.assertContains(response, 'Tyler M Patten')
        self.assertContains(response, '12:34:56:78:90'.upper())
        self.assertContains(response, 'ab:cd:ef:gh:hi'.upper())
        self.assertContains(response, '9876')

    def test_page_also_displays_accessories_only_for_that_asset(self):
        ##Create an accessory to search for.
        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        #Create an accessory
        correct_accessory = Accessory.objects.create(name='Unicorn Horn', asset=correct_asset)
        correct_accessory2 = Accessory.objects.create(name='Dragon Scales', asset=correct_asset)
        incorrect_accessory = Accessory.objects.create(name='Blackthorn Wand')

        all_assigned_accessories = Accessory.objects.filter(asset=correct_asset)
        response = self.client.get('/asset/%d/' % (correct_asset.tag,))

        self.assertEqual(response.context['accessories'][0], correct_accessory)
        self.assertEqual(response.context['accessories'][1], correct_accessory2)
        self.assertCountEqual(response.context['accessories'], all_assigned_accessories)


    def test_blank_entries_cannot_be_added(self):
         ##Create an accessory to search for.
        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        self.client.post(
            '/asset/%d/new' % (correct_asset.tag,),
            data={'accessory_name': ''}
        )

        response = self.client.get(
             '/asset/%d/' % (correct_asset.tag,),
        )

        all_accessories = Accessory.objects.all()
        self.assertEqual(all_accessories.count(), 0)
        for accessory in all_accessories:
            self.assertNotEqual(accessory.name, '')


    def test_asset_page_can_save_POST_request(self):
        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        self.client.post(
            '/asset/%d/new' % (correct_asset.tag,),
            data={'accessory_name': 'Solar Space Station'}
        )

        accessories = Accessory.objects.filter(asset=correct_asset)

        response = self.client.get(
             '/asset/%d/' % (correct_asset.tag,),
        )

        self.assertIn('Solar Space Station', response.content.decode())
        self.assertEqual(accessories.count(), 1)

    def test_accessory_add_redirects_after_POST(self):
        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')


        response = self.client.post(
            '/asset/%d/new' % (correct_asset.tag,),
            data={'accessory_name': 'Solar Space Station'}
        )

        self.assertRedirects(response, '/asset/%d/' % (correct_asset.tag,))

    def test_accessory_remove_accessory_can_save_on_POST(self):
        correct_asset = Asset.objects.create(tag=101,
                                             description='iPad',
                                             wifi_address='12:34:56:78:90',
                                             bluetooth_address='ab:cd:ef:gh:hi',
                                             type=self.create_type_for_assignment(),
                                             passcode='9876')

        removed_accessory = Accessory.objects.create(name="Rowan Wand", asset=correct_asset)


        self.client.post(
            '/asset/%d/remove' % (correct_asset.tag,),
            data={'accessory_id': removed_accessory.id}
        )

        accessories = Accessory.objects.filter(asset=correct_asset)

        response = self.client.get(
             '/asset/%d/' % (correct_asset.tag,),
        )

        self.assertNotIn('Rowan Wand', response.content.decode())
        self.assertEqual(accessories.count(), 0)


    def test_accessory_remove_redirects_after_POST(self):
            correct_asset = Asset.objects.create(tag=101,
                                                 description='iPad',
                                                 wifi_address='12:34:56:78:90',
                                                 bluetooth_address='ab:cd:ef:gh:hi',
                                                 type=self.create_type_for_assignment(),
                                                 passcode='9876')

            removed_accessory = Accessory.objects.create(name="Rowan Wand", asset=correct_asset)

            response = self.client.post(
                '/asset/%d/remove' % (correct_asset.tag,),
                data={'accessory_id': removed_accessory.id}
            )


            self.assertRedirects(response, '/asset/%d/' % (correct_asset.tag,))



class TestAssignmentHistoryDisplay(TestCase):
    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_displays_details_from_that_assignment_history(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        second_asset = Asset.objects.create(tag=3011836,
                                            description='Yes, Macbook',
                                            wifi_address='12:bb:cc:dd:12',
                                            bluetooth_address='12:bb:cc:dd:13',
                                            type=self.create_type_for_assignment(), )

        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date='2014-01-01',
                                                     end_date='2014-02-02',
                                                     note='This is a note.')

        second_assignment = Assignment.objects.create(person=first_person,
                                                      asset=second_asset,
                                                      start_date='2012-01-01',
                                                      end_date=None,
                                                      note='Quick notes.')

        response = self.client.get('/person/%s/' % (first_person.id,))

        self.assertContains(response, first_asset.tag)
        self.assertContains(response, '01-01-2014')
        self.assertContains(response, '02-02-2014')
        self.assertContains(response, second_asset.tag)
        self.assertContains(response, '01-01-2012')

    def test_that_asset_text_is_a_clickable_link(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        # #Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date='2014-01-01',
                                                     end_date='2014-02-02',
                                                     note='This is a note.')

        response = self.client.get('/person/%s/' % (first_person.id,))

        self.assertInHTML('<a href="/asset/%s/"> %s  </a>' % (first_asset.tag, first_asset.tag), str(response.content))


class TestAssignmentEndDate(TestCase):
    def create_type_for_assignment(self):
        # #Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def test_that_person_page_uses_form(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        ##Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date='2014-01-01',
                                                     end_date='2014-02-02',
                                                     note='This is a note.')

        response = self.client.get('/person/%s/' % (first_person.id,))
        for item in response.context['AssignmentForms']:
            self.assertIsInstance(item, AssignmentForm)


    def test_that_POST_can_add_end_date_to_assignment(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        ##Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date='2014-01-01',
                                                     note='This is a note.')

        end_date = timezone.now().date()
        data = {'end_date': end_date}
        af = AssignmentForm(data)
        af.instance = first_assignment
        response = self.client.post(
            '/assignment/{0}/end_date'.format(first_assignment.id),
            data={'end_date':  end_date.strftime(format="%m/%d/%Y")}
        )

        first_changed_assignment = Assignment.objects.get(pk=first_assignment.id)

        self.assertIsNotNone(first_changed_assignment.end_date)
        self.assertEqual(end_date, first_changed_assignment.end_date.date())


    def test_that_view_redirects_after_POST(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        ##Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date='2014-01-01',
                                                     note='This is a note.')

        end_date = timezone.now()
        data = {'end_date': end_date}
        af = AssignmentForm(data)
        af.instance = first_assignment
        response = self.client.post(
            '/assignment/{0}/end_date'.format(first_assignment.id),
            data={'end_date': end_date}
        )

        self.assertRedirects(response, '/person/{0}/'.format(first_person.id,))



    def test_that_POST_can_overwrite_date_to_blank(self):
        first_asset = Asset.objects.create(tag=3011835,
                                           description='Another Macbook',
                                           wifi_address='aa:bb:cc:dd:12',
                                           bluetooth_address='aa:bb:cc:dd:13',
                                           type=self.create_type_for_assignment(), )

        ##Create Person for Assignment
        first_person = Person.objects.create(id=1,
                                             last_name='Patten',
                                             first_name='Tyler',
                                             middle_name='Martin',
                                             student_number='12345123',
                                             birthdate='2014-09-01')

        first_assignment = Assignment.objects.create(person=first_person,
                                                     asset=first_asset,
                                                     start_date='2014-01-01',
                                                     note='This is a note.')

        end_date = None
        data = {'end_date': end_date}
        af = AssignmentForm(data)
        af.instance = first_assignment
        response = self.client.post(
            '/assignment/{0}/end_date'.format(first_assignment.id),
            data={'end_date':  None}
        )

        first_changed_assignment = Assignment.objects.get(pk=first_assignment.id)

        self.assertIsNone(first_changed_assignment.end_date)
