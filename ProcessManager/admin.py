from django.contrib import admin
from ProcessManager.models import Asset, Assignment, Person, DeviceType, Accessory, CheckIn, CheckInNote, DeviceCheckIn, AccessoryCheckIn
# Register your models here.

admin.site.register(Asset)
admin.site.register(Assignment)
admin.site.register(Person)
admin.site.register(DeviceType)
admin.site.register(Accessory)
admin.site.register(CheckIn)
admin.site.register(CheckInNote)
admin.site.register(DeviceCheckIn)
admin.site.register(AccessoryCheckIn)