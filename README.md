# README #

Our entire MLTI system needs a better management process.  Right now everything moves through multiple spreadsheets or systems- assets (JAMF), forms (Google Sheets), billing (Sheets), asset history (somewhere?) and student information (IC).  This will pull things together to facilitate better tracking, better check ins and check outs, and overall, a smoother inventory management process.


### What is this repository for? ###

* A TDD Django application designed to make it easy for our school to stay up to date with their laptop inventory.
* 0.5

### How do I get set up? ###
* Summary of set up
* All the configuration should be embedded in the repo.  
* Dependencies: Postgresql, Django, Selenium, Gunicorn, Nginx, Python 3.3+
* Postgres should be setup to accept connections.  All configuration files are included.
* Test are included in Test folder (both unit, and selenium driven functional tests.)
* You should be able to deploy using the included Fabric File.


### Who do I talk to? ###

*  Matt Frey-Davis, SIS Manager, RSU1 - mfreydavis@rsu1.org