from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'MLTIManager.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'ProcessManager.views.home_page', name='home_page'),
    url(r'^asset/(\d+)/new$', 'ProcessManager.views.add_accessory', name='add_accessory'),
    url(r'^asset/(\d+)/remove$', 'ProcessManager.views.remove_accessory', name='remove_accessory'),
    url(r'^asset/(\d+)/$', 'ProcessManager.views.asset', name='asset'),
    url(r'^search/asset/$', 'ProcessManager.views.search_asset', name='search_asset'),
    url(r'^search/person/$', 'ProcessManager.views.search_person', name='search_person'),
    url(r'^search/checkin/(\d+)/person/$', 'ProcessManager.views.checkin_search_person', name='checkin_search_person'),
    url(r'^person/(\d+)/$', 'ProcessManager.views.person', name='person'),
    url(r'^select_person/(\S+)/$', 'ProcessManager.views.select_person', name='select_person'),
    url(r'^person/$', 'ProcessManager.views.select_person', name='all_people'),
    url(r'^import', 'ProcessManager.views.import_files', name='import_files'),
    url(r'^assignment/(\d+)/end_date$', 'ProcessManager.views.assignment_end_date', name='assignment_end_date'),
    url(r'checkin/new$', 'ProcessManager.views.new_check_in', name='new_check_in'),
    url(r'checkin/(\d+)/$', 'ProcessManager.views.existing_check_in', name='existing_check_in'),
    url(r'checkin/(\d+)/(\d+)$', 'ProcessManager.views.device_check_in', name='device_check_in'),
    url(r'^checkin/(\d+)/select_person/(\S+)/$', 'ProcessManager.views.checkin_select_person', name='checkin_select_person'),
    url(r'^checkin/(\d+)/select_person/$', 'ProcessManager.views.checkin_select_person', name='checkin_select_person'),
    url(r'^checkin/(\d+)/(\d+)/save$', 'ProcessManager.views.save_check_in_results', name='check_in_save'),


    url(r'^admin/', include(admin.site.urls)),
)
