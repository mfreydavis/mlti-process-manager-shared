Provisioning a new site
=======================

## Required packages:

* nginx
* Python 3
* Git
* pip
* virtualenv

eg, on Ubuntu:

    sudo apt-get install nginx mercurial python3 python3-pip
    sudo pip3 install virtualenv
    sudo apt-get install postgresql
    sudo apt-get install python-psycopg2
    sudo apt-get install postgresql-server-dev-9.4

     
    sudo apt-get install nginx mercurial python3 python3-pip postgresql postgresql-server-dev-9.4 python python-psycopg2
## Create a database and a user
    sudo su postgres 
    psql -c "CREATE USER django PASSWORD 'django-test';"
    psql -c "CREATE DATABASE assets OWNER django;"
    
## Create the relevant folders
    export SITENAME=assets.testing.rsu1.org
    mkdir -p ~/sites/$SITENAME/database
    mkdir -p ~/sites/$SITENAME/static
    mkdir -p ~/sites/$SITENAME/virtualenv
    hg clone 'https://bitbucket.org/rsu1_mfreydavis/mlti-process-manager' ~/sites/$SITENAME/source

##Move the configs up
    scp /Users/matt/PycharmProjects/MLTIManager/deploy_tools/gunicorn-upstart.template.conf techdept@assets.testing.rsu1.org:~/
    scp /Users/matt/PycharmProjects/MLTIManager/deploy_tools/nginx.template.conf techdept@assets.testing.rsu1.org:~/


## Nginx Virtual Host config

* Location example: /etc/nginx/sites-available/superlists-staging.ottg.eu
* see nginx.template.conf
* replace SITENAME with, eg, staging.my-domain.com
* Enable sudo ln -s /etc/nginx/sites-available/$SITENAME /etc/nginx/sites-enabled/$SITENAME

## Upstart Job

* Location example: /etc/init/gunicorn-superlists-staging.ottg.eu.conf
* see gunicorn-upstart.template.conf
* replace SITENAME with, eg, staging.my-domain.com

## Folder structure:
Assume we have a user account at /home/username

/home/username
└── sites
    └── SITENAME
         ├── database
         ├── source
         ├── static
         └── virtualenv
         
 ## Fabric:
 * Switch to virtenv that is python2 w/fabric installed
 * Often at ~/fabric
 * Run fab deploy:host=techdept@SITENAME;
  source ~/fabric/bin/activate
  cd /Users/matt/PycharmProjects/MLTIManager/deploy_tools
  fab deploy:host=techdept@assets.testing.rsu1.org
 
 