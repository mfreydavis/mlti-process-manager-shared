from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys
from ProcessManager.models import Asset, Person, Note, Assignment, DeviceType

from django.utils import timezone

import datetime

import sys
# Create your tests here.


class TechnicianSearchesPersonByLastName(FunctionalTest):


    def test_can_search_for_a_person_by_last_name(self):
        #Tyler needs to go to look up the record for a person, Sally Smith.
        #So, he goes to the website.
        self.browser.get(self.server_url)

        #On there, he enters he sees the Person search box, inviting him to search using the placeholder "Search for a Person"
        inputbox = self.browser.find_element_by_id('id_person_search_box')
        self.assertEqual(inputbox.get_attribute('placeholder'),
                         'Search for a Person'
        )

       ##Create Dummy Person
        correct_person = Person.objects.create(
                                               last_name='Smith',
                                               first_name='Sally',
                                               middle_name='Middle',
                                               student_number='12345678',
                                               birthdate='2014-08-18',
                                               )

        ##Make a note and attach it to the person.
        note = Note.objects.create(person=correct_person, content='She is a wonderful person.')

        ##Make two items in assignment history and check for it.
        first_asset = Asset.objects.create(tag=3011835,
                                             description='Another Macbook',
                                             wifi_address='aa:bb:cc:dd:12',
                                             bluetooth_address='aa:bb:cc:dd:13',
                                             type=self.create_type_for_assignment())

        second_asset = Asset.objects.create(tag=3011836,
                                             description='Yes, Macbook',
                                             wifi_address='12:bb:cc:dd:12',
                                             bluetooth_address='12:bb:cc:dd:13',
                                             type=self.create_type_for_assignment(),)


        ##Assign those assets.
        first_device_assignment = Assignment.objects.create(person=correct_person, asset = first_asset,
                                                            start_date = timezone.now() - datetime.timedelta(days=5),
                                                            note='Normal Assignment')
        second_device_assignment = Assignment.objects.create(person=correct_person, asset = second_asset,
                                                             start_date = timezone.now() - datetime.timedelta(weeks=104),
                                                             end_date=timezone.now() - datetime.timedelta(weeks=52), note='Expired Assignment')

        #He enters in the word "Smith" and hits enter, which
        inputbox.send_keys('Smith')
        inputbox.send_keys(Keys.ENTER)


        #it takes him directly to the only match for that person. titled after the name of the person, ie, Sally M. Smith
        person_url = self.browser.current_url
        self.assertRegex(person_url, '/person/\d+/')

        #This page displays name, demographics, enrollment, and
        #any devices that are assigned to that person. in several neat grids
        self.check_for_row_in_person_table('Name Sally M Smith')
        self.check_for_row_in_person_table('Student Number 12345678')
        self.check_for_row_in_person_table('Date of Birth Aug. 18, 2014')



        #And notes about that person, saying "She is a wonderful person."
        notes = self.browser.find_element_by_id('id_notes_box')
        self.assertEqual(notes.text, 'She is a wonderful person.')


        #As well as the device assignment history, which displays as a list of past assignments, each row looking like:
        # Asset # | Type | Start Date | End Date with any active devices showing active in the display.
        #asset_history = self.browser.find_element_by_id('id_asset_history_table')

        table = self.browser.find_element_by_id('id_asset_history_table')
        rows = table.find_elements_by_tag_name('thead')

        self.assertIn('Asset Tag Type Start Date End Date', [row.text for row in rows])

        #If Sally has more than one device both show up.
        self.check_for_row_in_asset_history_table('3011835 Macbook %s' % (timezone.now() - datetime.timedelta(days=5)).strftime(format="%m-%d-%Y"))
        self.check_for_row_in_asset_history_table('3011836 Macbook %s %s' % ((timezone.now() - datetime.timedelta(weeks=104)).strftime(format="%m-%d-%Y"),
                                                                               (timezone.now() - datetime.timedelta(weeks=52)).strftime(format="%m-%d-%Y")))
        active = self.browser.find_element_by_css_selector('tr.active')
        self.assertEqual(active.text, '3011835 Macbook %s' % (timezone.now() - datetime.timedelta(days=5)).strftime(format="%m-%d-%Y"))

        self.browser.save_screenshot('/Users/matt/PycharmProjects/MLTIManager/person_screen.png')

        #Tyler clicks on the device assigned and it brings him to the device page.

        link = self.browser.find_element_by_link_text('3011835')
        link.click()

        asset_url = self.browser.current_url

        ##Does this need to be improved?  ##Refactor to URL someday
        self.assertRegex(asset_url, '/asset/3011835/' )


        #Tyler searches again, this time for Newcomb and it brings up multiple people who are in the system.
        ##Create those two people.
        first_person = Person.objects.create(
                                       last_name='Newcomb',
                                       first_name='Sally-1',
                                       middle_name='Middle',
                                       student_number='12345678',
                                       birthdate='2014-08-18',
                                       )

        second_person = Person.objects.create(id=10,
                                       last_name='Newcomb',
                                       first_name='Sally-2',
                                       middle_name='Middle',
                                       student_number='11111111',
                                       birthdate='2014-07-18',
                                       )

        inputbox = self.browser.find_element_by_id('id_person_search_box')
        inputbox.send_keys('Newcomb')
        inputbox.send_keys(Keys.ENTER)

        #This new page is , titled Select Person where he sees either the list of people with the last name Newcomb,
        self.assertEqual("Select Person", self.browser.title)
        person_url = self.browser.current_url
        self.assertRegex(person_url, '/select_person/(\D+)')

        #These are displayed in a neat and easy to use table with clickable links to each record,
        # titled "Person Search", the unique URL contains their internal ID number, giving, hopefully, a stable scheme.
        table = self.browser.find_element_by_id('id_person_table')
        self.check_for_row_in_person_table('Sally-1 M Newcomb 12345678 08-18-2014')
        self.check_for_row_in_person_table('Sally-2 M Newcomb 11111111 07-18-2014')

        #Clicking on  of these links brings him to a the person record.
        link = self.browser.find_element_by_id('id_person_link_10')
        link.click()

        person_url = self.browser.current_url

        self.assertRegex(person_url, '/person/%s/' % (second_person.id,))

        #Finally, he searches for a person who isn't there. Doe, Jane


        inputbox = self.browser.find_element_by_id('id_person_search_box')
        inputbox.send_keys('Doe')
        inputbox.send_keys(Keys.ENTER)

        #That brings him to the same page as when you find multiple matches
        self.assertEqual("Select Person", self.browser.title)
        person_url = self.browser.current_url
        self.assertRegex(person_url, '/select_person/Doe')


        #The big difference is this this shows everyone, displaying the number of matches
        all_people = Person.objects.all()
        number_of_people = all_people.count()

        browser_count = self.browser.find_element_by_id('id_result_count')
        self.assertEqual("(%s Options)" % (number_of_people,) , browser_count.text)



        #Satisfied that he can find someone, he closes out and is done.





