from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys
from ProcessManager.models import Asset, Person, Note, Assignment, DeviceType

from django.utils import timezone

import datetime

import sys
# Create your tests here.


class TechnicianSearchesByStudentNumber(FunctionalTest):

    def test_can_search_for_a_person_by_student_number(self):
       ##Create Dummy Person
        correct_person = Person.objects.create(id=1,
                                               last_name='Smith',
                                               first_name='Sally',
                                               middle_name='Middle',
                                               student_number='12345678',
                                               birthdate='2014-08-18',
                                           )
        #Tyler wants to see if he can search for a person by student ID, he goes to the website
        self.browser.get(self.server_url)

        #And locates the input box
        inputbox = self.browser.find_element_by_id('id_person_search_box')


        #There he types in their student id 12345678
        inputbox.send_keys('12345678')
        inputbox.send_keys(Keys.ENTER)

        person_url = self.browser.current_url
        #This brings them to their person display page.
        self.assertRegex(person_url, '/person/1/')




