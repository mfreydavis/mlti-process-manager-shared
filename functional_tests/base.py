from selenium import webdriver
from ProcessManager.models import Asset, Person, Note, Assignment, DeviceType
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test.utils import override_settings
from selenium.webdriver.support.ui import WebDriverWait



import sys
# Create your tests here.

@override_settings(DEBUG=True)
class FunctionalTest(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        for arg in sys.argv:
            if 'liveserver' in arg:
                cls.server_url = 'http://' + arg.split('=')[1]
                return
        super().setUpClass()
        cls.server_url = cls.live_server_url

    @classmethod
    def tearDownClass(cls):
        if cls.server_url == cls.live_server_url:
            super().tearDownClass()

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_if_row_in_table_has_input_group(self, row):
        try:
            row.find_element_by_class_name('input-group')
            return True
        except:
            return False

    def check_for_row_in_asset_table(self, row_text):
        table = self.browser.find_element_by_id('id_asset_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def create_type_for_assignment(self):
        ##Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')

    def check_for_row_in_person_table(self, row_text):
        table = self.browser.find_element_by_id('id_person_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def check_for_row_in_asset_history_table(self, row_text):
        table = self.browser.find_element_by_id('id_asset_history_table')
        rows = table.find_elements_by_tag_name('tr')
        #Find the value between quotes.
        computed_rows = []
        for row in rows:
            if self.check_if_row_in_table_has_input_group(row):

                value_info = row.find_element_by_class_name('input-group').get_attribute('innerHTML')
                ##Find just the row information.
                value = value_info[value_info.find('value="') + len('value="'):  value_info.find('value="') + len('value="') +
                                                                                 value_info[value_info.find('value="') + len('value="'):].find('"') ]
                if len(value) > 0:
                    computed_rows.append(row.text + ' ' + value.strip())
                else:
                    computed_rows.append(row.text)

            else:
                computed_rows.count(row.text)

        self.assertIn(row_text, computed_rows)

    def check_for_row_in_accessory_table(self, row_text):
        accessibility_text = '\nAccessory Name'
        table = self.browser.find_element_by_id('id_accessory_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text + accessibility_text, [row.text for row in rows])

    def check_for_row_not_in_accessory_table(self, row_text):
        accessibility_text = '\nAccessory Name'
        table = self.browser.find_element_by_id('id_accessory_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertNotIn(row_text + accessibility_text, [row.text for row in rows])

    def check_for_row_in_check_in_table(self, row_text):
        table = self.browser.find_element_by_id('id_check_in_table')
        rows = table.find_elements_by_tag_name('td')

        self.assertIn(row_text, [row.text for row in rows])



    def create_type_for_assignment(self):
        ##Create a device type
        return DeviceType.objects.create(name='Macbook', description='2013 Macbook Air')
