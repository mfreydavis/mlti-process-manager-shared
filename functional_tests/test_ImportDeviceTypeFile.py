__author__ = 'matt'

from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys
from ProcessManager.models import Asset, Person, Note, Assignment, DeviceType
import csv
import time

class TechnicianImportDeviceTypeCSV(FunctionalTest):

    def test_can_upload(self):
        ##Tyler visits the website and clicks on the link for Imports.
        self.browser.get(self.server_url)
        self.browser.set_window_size(1024, 768)

        link = self.browser.find_element_by_id('id_import').click()

        import_url = self.browser.current_url

        ##Does this need to be improved?  ##Refactor to URL someday
        self.assertRegex(import_url, '/import')

        #The title says "Import Setup Information"
        self.assertEquals( 'Import Data Files', self.browser.title)

        ##And he sets the correct upload type
        self.browser.find_element_by_xpath('//*[@id="id_upload_type"]/option[text()="Device Types"]').click()

        ##He sees multiple options, including one toward the top that offers to let him import a file.
        button = self.browser.find_element_by_id('id_select_person_upload')
       ## self.assertEqual('Choose File', button.text)

        ##He clicks on the button labeled "Select File to Upload"
        ##button.click()

        ##And chooses a file to upload.

        button.send_keys('/Users/matt/PycharmProjects/MLTIManager/functional_tests/test_files/DeviceType.csv')
        button.submit()

        ##That file then is uploaded, and a message is returned, stating that it was successful with the number of records imported..
        time.sleep(2)


        found_type = DeviceType.objects.get(name='MacBook Air 11')
        self.assertEquals(found_type.description, 'MLTI Device')

        self.assertIn("Success. 6 created, 0 updated, 0 failed.", self.browser.find_element_by_id('id_process_message').text)

        ##Satisfied, he leaves, happy.

        ##Test Unsuccessful Use Case##

    def test_returns_the_correct_number_of_uploaded_records(self):
        ##Tyler visits the website and clicks on the link for Imports.
        self.browser.get(self.server_url)
        self.browser.set_window_size(1024, 768)

        link = self.browser.find_element_by_id('id_import').click()

        import_url = self.browser.current_url


        #The title says "Import Setup Information"
        self.assertEquals( 'Import Data Files', self.browser.title)

        ##Select the right upload option.
        self.browser.find_element_by_xpath('//*[@id="id_upload_type"]/option[text()="Device Types"]').click()

        ##He sees multiple options, including one toward the top that offers to let him import a file.
        button = self.browser.find_element_by_id('id_select_person_upload')

        ##He clicks on the button labeled "Select File to Upload"
        ##button.click()
        ##And chooses a file to upload.

        #Establish a device to change.
        DeviceType.objects.create(name='MacBook Air 11', description="Early MacBook Laptop")

        with open('functional_tests/test_files/generated_files/f_DeviceType.csv', 'w') as inFile:
            tempCSV = csv.writer(inFile)
            # Abed,Faris,MiddleName,130130220,7/22/98 00:00
            tempCSV.writerow(['Name','Description'])
            tempCSV.writerow(['Acer c720p', 'Laptops purchased to and given to the science department.'])
            tempCSV.writerow(['MacBook Air 11', 'Tiny MacBook for Student Teachers'])
            tempCSV.writerow(['MacBook Air 13', 'The Normal MacBook'])


        button.send_keys('/Users/matt/PycharmProjects/MLTIManager/functional_tests/test_files/generated_files/f_DeviceType.csv')
        button.submit()

        ##Wait for upload
        time.sleep(2)

        ##
        self.assertIn("Success. 2 created, 1 updated, 0 failed.", self.browser.find_element_by_id('id_process_message').text)




