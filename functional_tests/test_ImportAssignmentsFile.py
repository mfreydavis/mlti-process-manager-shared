__author__ = 'matt'

from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys
from ProcessManager.models import Asset, Person, Note, Assignment, DeviceType
from ProcessManager.forms import DataUploadForm
import csv
import time

class TechnicianImportAssignmentCSV(FunctionalTest):

    def test_can_upload(self):
        ##Tyler visits the website and clicks on the link for Imports.

        ##Pull in and load Assets Types, a dependency
        form = DataUploadForm()
        file_loc = 'functional_tests/test_files/Assets.csv'
        form.file = file_loc

        with open(file_loc, 'rb') as fcsv:
            self.client.post('/import', {'upload_type' : 'Assets', 'file' : fcsv})

        #Load Persons, a dependency
        form = DataUploadForm()
        file_loc = 'functional_tests/test_files/Persons.csv'
        form.file = file_loc

        with open(file_loc, 'rb') as fcsv:
            self.client.post('/import', {'upload_type' : 'Person', 'file' : fcsv})



        ##Now start testing.
        self.browser.get(self.server_url)
        self.browser.set_window_size(1024, 768)

        link = self.browser.find_element_by_id('id_import').click()

        import_url = self.browser.current_url

        ##Does this need to be improved?  ##Refactor to URL someday
        self.assertRegex(import_url, '/import')

        #The title says "Import Setup Information"
        self.assertEquals( 'Import Data Files', self.browser.title)

        ##And he sets the correct upload type
        self.browser.find_element_by_xpath('//*[@id="id_upload_type"]/option[text()="Device Assignments"]').click()

        ##He sees multiple options, including one toward the top that offers to let him import a file.
        button = self.browser.find_element_by_id('id_select_person_upload')
       ## self.assertEqual('Choose File', button.text)

        ##He clicks on the button labeled "Select File to Upload"
        ##button.click()

        ##And chooses a file to upload.

        button.send_keys('/Users/matt/PycharmProjects/MLTIManager/functional_tests/test_files/DeviceAssignments.csv')
        button.submit()

        ##That file then is uploaded, and a message is returned, stating that it was successful with the number of records imported..
        time.sleep(2)


        ##He searches for device 4016966, which has been paired with 106622218.

        inputbox = self.browser.find_element_by_id('id_search_box')
        inputbox.send_keys('4016966')
        inputbox.send_keys(Keys.ENTER)

        ##He checks to find that information
        self.check_for_row_in_asset_table('Assignment Ciera Williams')

        ##Satisfied, he leaves, happy.


#@Todo test unsuccessful case
#@Todo Test correct number of results.
    # def test_returns_the_correct_number_of_uploaded_records(self):
    #
    #     ##Pull in and load Device Types, a dependency
    #     form = DataUploadForm()
    #     file_loc = 'functional_tests/test_files/DeviceType.csv'
    #     form.file = file_loc
    #
    #     with open(file_loc, 'rb') as fcsv:
    #         self.client.post('/import', {'upload_type' : 'DeviceType', 'file' : fcsv})
    #
    #     ##Tyler visits the website and clicks on the link for Imports.
    #     self.browser.get(self.server_url)
    #     self.browser.set_window_size(1024, 768)
    #
    #     link = self.browser.find_element_by_id('id_import').click()
    #
    #     import_url = self.browser.current_url
    #
    #
    #     #The title says "Import Setup Information"
    #     self.assertEquals( 'Import Data Files', self.browser.title)
    #
    #     ##Select the right upload option.
    #     self.browser.find_element_by_xpath('//*[@id="id_upload_type"]/option[text()="Assets"]').click()
    #
    #     ##He sees multiple options, including one toward the top that offers to let him import a file.
    #     button = self.browser.find_element_by_id('id_select_person_upload')
    #
    #     ##He clicks on the button labeled "Select File to Upload"
    #     ##button.click()
    #     ##And chooses a file to upload.
    #
    #     #Establish a device to change.
    #     Asset.objects.create(tag='2011634', type=DeviceType.objects.get(name='MacBook Air 11'), serial_number='C0123',
    #                          description='', wifi_id='85:38:35:57:E6:02', bluetooth_id = '85:38:35:57:E6:03',
    #                          passcode = '1092')
    #
    #     with open('functional_tests/test_files/generated_files/f_DeviceType.csv', 'w') as inFile:
    #         tempCSV = csv.writer(inFile)
    #         ##Tag,Type,Description,"Serial Number","WiFi ID","Bluetooth ID",Passcode
    #         ##2011634,"MacBook Air 11",,C02L11UJFH51,84:38:35:57:E6:02,84:38:35:57:E6:03,
    #         tempCSV.writerow(['Tag','Type', 'Description', 'Serial Number', 'WiFi ID', 'Bluetooth ID', 'Passcode'])
    #         tempCSV.writerow(['2011634', 'MacBook Air 13','','C02L11UJFH51',
    #                           '84:38:35:57:E6:02','84:38:35:57:E6:03','0000'])
    #         tempCSV.writerow(['2011635', 'MacBook Air 11','','C02L11UJFH51',
    #                           '84:38:35:57:E6:12','84:38:35:57:E6:23','0020'])
    #         tempCSV.writerow(['2011636', 'MacBook Air 11','','C02L11UJFH51',
    #                           '84:38:35:57:E6:22','84:38:35:57:E6:33','0020'])
    #
    #
    #     button.send_keys('/Users/matt/PycharmProjects/MLTIManager/functional_tests/test_files/generated_files/f_DeviceType.csv')
    #     button.submit()
    #
    #     ##Wait for upload
    #     time.sleep(2)
    #
    #     ##
    #     self.assertIn("Success. 2 created, 1 updated, 0 failed.", self.browser.find_element_by_id('id_process_message').text)




