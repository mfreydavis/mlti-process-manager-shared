__author__ = 'matt'

from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys
from ProcessManager.models import Asset, Person, Assignment, Accessory
from django.utils import timezone
import datetime


class AssetAssignmentTest(FunctionalTest):

    def test_person_can_end_date_assets_from_person_page(self):
        ###Create Dummy Person
        correct_person = Person.objects.create(
                                               last_name='Smith',
                                               first_name='Sally',
                                               middle_name='Middle',
                                               student_number='12345678',
                                               birthdate='2014-08-18',
                                               )

        ##Make one asset and assign it.
        first_asset = Asset.objects.create(tag=3011835,
                                             description='Another Macbook',
                                             wifi_address='aa:bb:cc:dd:12',
                                             bluetooth_address='aa:bb:cc:dd:13',
                                             type=self.create_type_for_assignment())

        second_asset = Asset.objects.create(tag=3011836,
                                             description='Another Macbook',
                                             wifi_address='aa:bb:cc:dc:12',
                                             bluetooth_address='aa:bb:cc:dc:13',
                                             type=self.create_type_for_assignment())

        first_device_assignment = Assignment.objects.create(person=correct_person, asset = first_asset,
                                                    start_date = timezone.now() - datetime.timedelta(days=5),
                                                    note='Normal Assignment')


        #Tyler opens up the site.
        self.browser.get(self.live_server_url)

        #He types Smith into the asset search box to look for Sally.
        inputbox = self.browser.find_element_by_id('id_person_search_box')
        inputbox.send_keys('Smith')
        inputbox.send_keys(Keys.ENTER)

        #On that page he sees that she is assigned that asset in the asset table.
        self.check_for_row_in_asset_history_table('3011835 Macbook %s' % (timezone.now() - datetime.timedelta(days=5)).strftime(format="%m-%d-%Y"))

        #He finds the box at the end of the row.
        end_date = self.browser.find_element_by_id('id_add_end_date_box_{0}'.format(first_device_assignment.id))

        #He sets the end date to be five days from now.
        end_date.send_keys((timezone.now() + datetime.timedelta(days=5)).strftime(format="%m/%d/%Y"))

        #He hits the submit button
        self.browser.find_element_by_id('id_add_end_date_{0}'.format(first_device_assignment.id)).click()
        #And then the page updates to show the new end date.
        self.check_for_row_in_asset_history_table('3011835 Macbook {0} {1}'.format((timezone.now() - datetime.timedelta(days=5)).strftime(format="%m-%d-%Y"), (timezone.now() + datetime.timedelta(days=5)).strftime(format="%m-%d-%Y")))

        #Satisifed that the asset has been end dated, he's all set.


    #@TODO
    def test_person_can_assign_asset_from_person_page(self):
        self.fail("Write the test.")

    #@TODO
    def test_person_can_assign_assets_from_asset_page(self):
        self.fail("Write the test.")

    #@TODO
    def test_person_can_end_date_asset_from_asset_page(self):
        self.fail("Write the test.")