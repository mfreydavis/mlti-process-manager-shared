__author__ = 'matt'

from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys
from ProcessManager.models import Asset, Person, Assignment, Accessory
from django.utils import timezone
import datetime

class AccessoryAccessTest(FunctionalTest):

    def test_person_can_add_accessories_and_view_them(self):

        #Tyler is looking to see what accessories a person has, so he goes to the website.
        self.browser.get(self.server_url)


       ##Create Dummy Person
        correct_person = Person.objects.create(
                                               last_name='Smith',
                                               first_name='Sally',
                                               middle_name='Middle',
                                               student_number='12345678',
                                               birthdate='2014-08-18',
                                               )

        ##Make one asset and assign it.
        first_asset = Asset.objects.create(tag=3011835,
                                             description='Another Macbook',
                                             wifi_address='aa:bb:cc:dd:12',
                                             bluetooth_address='aa:bb:cc:dd:13',
                                             type=self.create_type_for_assignment())

        second_asset = Asset.objects.create(tag=3011836,
                                             description='Another Macbook',
                                             wifi_address='aa:bb:cc:dc:12',
                                             bluetooth_address='aa:bb:cc:dc:13',
                                             type=self.create_type_for_assignment())


        Accessory.objects.create(name='Sonic Screwdriver', asset=first_asset)

        #He types 3011835 into the asset search box to look for the asset.
        inputbox = self.browser.find_element_by_id('id_search_box')
        inputbox.send_keys('3011835')
        inputbox.send_keys(Keys.ENTER)

        #This brings him to the page where he goes to view the record, and sees she has an Sonic Screwdriver.
        self.check_for_row_in_accessory_table('Sonic Screwdriver')


        #Seeing that the standard accessories are missing, he goes to add them in.
        #There is an inviting plus button at the end of the line, inviting him to add an accessory.
        #And he types in the new standard accessory and hits enter.
        inputbox = self.browser.find_element_by_id('id_add_accessory_box')
        inputbox.send_keys('Tardis')
        inputbox.send_keys(Keys.ENTER)

        #The page updates and he now sees the accessory.
        self.check_for_row_in_accessory_table('Tardis')


        #He also wants to go to another unrelated asset and add an accessory there.
        inputbox = self.browser.find_element_by_id('id_search_box')
        inputbox.send_keys('3011836')
        inputbox.send_keys(Keys.ENTER)


        #On that page he sees no indication that the person has the accessories that were assigned to the other person.
        self.check_for_row_not_in_accessory_table('Sonic Screwdriver')
        self.check_for_row_not_in_accessory_table('Tardis')

        #Tyler accidentally clicks on a row when it is empty and adds a new item.
        #He clicks it.
        plusbutton = self.browser.find_element_by_id('id_add_accessory')
        plusbutton.click()

        #No new row appears, since it has empty input.
        self.check_for_row_not_in_accessory_table('')


        #Satisfied he leaves.

    def test_person_can_remove_accessories(self):

        #Tyler is looking to see what accessories a person has, so he goes to the website.
        self.browser.get(self.server_url)


       ##Create Dummy Person
        correct_person = Person.objects.create(
                                               last_name='Smith',
                                               first_name='Sally',
                                               middle_name='Middle',
                                               student_number='12345678',
                                               birthdate='2014-08-18',
                                               )

        ##Make one asset and assign it.
        first_asset = Asset.objects.create(tag=3011835,
                                             description='Another Macbook',
                                             wifi_address='aa:bb:cc:dd:12',
                                             bluetooth_address='aa:bb:cc:dd:13',
                                             type=self.create_type_for_assignment())

        second_asset = Asset.objects.create(tag=3011836,
                                             description='Another Macbook',
                                             wifi_address='aa:bb:cc:dc:12',
                                             bluetooth_address='aa:bb:cc:dc:13',
                                             type=self.create_type_for_assignment())


        accessory = Accessory.objects.create(name='Sonic Screwdriver', asset=first_asset)

        #He types Smith into the asset search box to look for Sally.
        inputbox = self.browser.find_element_by_id('id_search_box')
        inputbox.send_keys('3011835')
        inputbox.send_keys(Keys.ENTER)

        #This brings him to the page where he goes to view the record, and sees she has an Sonic Screwdriver.
        self.check_for_row_in_accessory_table('Sonic Screwdriver')

        #This person should no longer get the Sonic Screwdriver, so Tyler clicks on the X button at the end.
        minussign = self.browser.find_element_by_id('id_remove_accessory_'+ str(accessory.id))
        minussign.click()

        #Then it no longer shows up on that page.
        ##Include the accessiblity text.
        self.check_for_row_not_in_accessory_table('Sonic Screwdriver')


        #Satisfied he leaves.


