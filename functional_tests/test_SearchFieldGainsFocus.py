from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys
from ProcessManager.models import Asset, Person, Note, Assignment, DeviceType

# Create your tests here.

class SearchFieldGainsFocusTest(FunctionalTest):
    def test_search_field_gets_focus(self):
        self.browser.get(self.server_url)
        self.browser.set_window_size(1024, 768)


        ##Tyler visits the website and the focus automatically goes to the search box

        focusbox = self.browser.switch_to.active_element

        inputbox = self.browser.find_element_by_id('id_search_box')
        ##This true, he can happily scan with a barcode scanner.
        self.assertEqual(focusbox, inputbox)

