__author__ = 'matt'

from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys
from ProcessManager.models import Asset, Person, Assignment, Accessory, CheckIn
from django.utils import timezone
from time import sleep
import datetime

class CheckinCreationAndManagement(FunctionalTest):

    def test_person_can_create_new_checkin(self):
        ##Create Dummy Person
        correct_person = Person.objects.create(id=1,
                                               last_name='Smith',
                                               first_name='Sally',
                                               middle_name='Middle',
                                               student_number='12345678',
                                               birthdate='2014-08-18',
                                           )
        first_asset = Asset.objects.create(tag=3011835,
                                             description='Another Macbook',
                                             wifi_address='aa:bb:cc:dd:12',
                                             bluetooth_address='aa:bb:cc:dd:13',
                                             type=self.create_type_for_assignment())

        first_device_assignment = Assignment.objects.create(person=correct_person, asset = first_asset,
                                                    start_date = timezone.now() - datetime.timedelta(days=5),
                                                    note='Normal Assignment')


        accessory1 = Accessory.objects.create(name='Sonic Screwdriver', asset=first_asset)
        accessory2 = Accessory.objects.create(name='Tardis', asset=first_asset)
        accessory3 = Accessory.objects.create(name='Anti-oil', asset=first_asset)

        #Tyler is looking to do a checkin, so he goes to the dashboard.
        self.browser.get(self.server_url)

        #On that page, he sees a section for "Check In"
        #He clicks the button, "+ New Checkin at the top.
        new_checkin_button = self.browser.find_element_by_id('id_new_checkin')
        self.assertInHTML('New Checkin', new_checkin_button.text)
        new_checkin_button.click()

        #Which brings up a modal for him to enter in the information about the checkin
        #He fills out the form, settings a start of today,
        # and end date of five days from today, and some notes.
        start_check = (timezone.now())
        end_check = (timezone.now() + datetime.timedelta(days=5))
        note_check = 'A normal check up during the year.'
        ##Clear the prefilled information.
        self.browser.find_element_by_id('id_start_date').clear()
        self.browser.find_element_by_id('id_start_date').send_keys(start_check.strftime(format="%m/%d/%Y"))
        self.browser.find_element_by_id('id_end_date').send_keys(end_check.strftime(format="%m/%d/%Y"))
        self.browser.find_element_by_id('id_note').send_keys(note_check)

        #And he hits submit
        self.browser.find_element_by_id('id_new_checkin_modal_submit').click()


        check = CheckIn.objects.first()
        #The page refreshes, and a new checkin appears.
        start_date = self.browser.find_element_by_id('id_start_date_{0}'.format(check.id))
        end_date = self.browser.find_element_by_id('id_end_date_{0}'.format(check.id))
        note = self.browser.find_element_by_id('id_note_{0}'.format(check.id))

        self.assertEqual(start_check.strftime(format="%B %-d, %Y"), start_date.text)
        self.assertEqual(end_check.strftime(format="%B %-d, %Y"), end_date.text)
        self.assertEqual(note_check, note.text)

        #There is also a progress bar.
        progress_bar = self.browser.find_element_by_id('id_progress_bar_{0}'.format(check.id))

        #Tyler clicks on that table_row bar and it brings him to the checkin in progress.
        self.browser.find_element_by_id('id_check_in_status_row_{0}'.format(check.id)).click()
        checkin_url = self.browser.current_url
        self.assertRegex(checkin_url, '/checkin/\d+/')

        #From there he has his normal search fields at the top, but instead of redirecting him to the page
        #It searches that set of people and brings up the checkin page.
        search_box = self.browser.find_element_by_id('id_person_search_box')
        self.assertEqual(search_box.get_attribute('placeholder'),
                         'Search for a Person'
                         )

        search_box.send_keys('Smith')
        search_box.send_keys(Keys.ENTER)

        #This returns him an exact match.
        checkin_url = self.browser.current_url
        self.assertRegex(checkin_url, '/checkin/\d+/\d+')


        #And displays information about that student and their assignment.
        self.check_for_row_in_person_table('Name Sally M Smith')
        self.check_for_row_in_person_table('Student Number 12345678')
        self.check_for_row_in_person_table('Date of Birth Aug. 18, 2014')

        #On that page he sees all the accessories listed
        self.check_for_row_in_check_in_table('Sonic Screwdriver')
        self.check_for_row_in_check_in_table('Tardis')
        self.check_for_row_in_check_in_table('Anti-oil')

        #He also sees the asset listed{{ accessory.id }}
        self.check_for_row_in_check_in_table(first_asset.type.name)



        #On the right of that, he sees a summary box
        summary_box = self.browser.find_element_by_id('id_check_in_summary')

        #And a box for notes.
        note_box = self.browser.find_element_by_id('id_notes-content')

        #Tyler enters in information about each accessory.
        #Next to them there are buttons that allow them to check the status of the device.
        #He chooses a status for each.
        asset_present = self.browser.find_element_by_id('id_devices-status_1').find_element_by_xpath('..')
        accessory1_missing = self.browser.find_element_by_id('id_accessories-0-status_2').find_element_by_xpath('..')
        accessory2_damaged = self.browser.find_element_by_id('id_accessories-1-status_0').find_element_by_xpath('..')
        accessory3_damaged = self.browser.find_element_by_id('id_accessories-2-status_0').find_element_by_xpath('..')
        accessory3_warranty = self.browser.find_element_by_id('id_accessory_warranty_{1}'.format('warranty', accessory3.id))

        accessory3_missing = self.browser.find_element_by_id('id_accessories-2-status_2').find_element_by_xpath('..')

        asset_present.click()
        accessory1_missing.click()
        accessory2_damaged.click()
        accessory3_damaged.click()
        accessory3_warranty.click()

        #They all become active and selected in the parent class.
        self.assertIn('active', asset_present.get_attribute("class"))
        self.assertIn('active', accessory1_missing.get_attribute("class"))
        self.assertIn('active', accessory2_damaged.get_attribute("class"))
        self.assertIn('active', accessory3_damaged.get_attribute("class"))
        self.assertIn('active', accessory3_warranty.get_attribute("class"))

        #But there aren't others that are selected.
        self.assertNotIn('active', accessory3_missing.find_element_by_xpath('..').get_attribute("class"))


        #THis includes damage on an iPad, where he can click to bring up a modal with a webcam to take a photo.
        #@TODO ADD THIS IN LATER

        #And enters in a note.
        note_text = 'This is a note about the status of the check in and other such information.'
        note_box.send_keys(note_text)



        #Then hits save.
        self.browser.find_element_by_id('id_save_button_dropdown').click()
        self.browser.find_element_by_id('id_save').click()

        #He checks the results by reloading the page...
        self.browser.refresh()

        #and sees that all the buttons are still checked.
        asset_present = self.browser.find_element_by_id('id_devices-status_1').find_element_by_xpath('..')
        accessory1_missing = self.browser.find_element_by_id('id_accessories-0-status_2').find_element_by_xpath('..')
        accessory2_damaged = self.browser.find_element_by_id('id_accessories-1-status_0').find_element_by_xpath('..')
        accessory3_damaged = self.browser.find_element_by_id('id_accessories-2-status_0').find_element_by_xpath('..')
        accessory3_warranty = self.browser.find_element_by_id('id_accessory_warranty_{1}'.format('warranty', accessory3.id))
        accessory3_missing = self.browser.find_element_by_id('id_accessories-2-status_2').find_element_by_xpath('..')
        note_box = self.browser.find_element_by_id('id_notes-content')


        self.assertIn('active', asset_present.get_attribute("class"))
        self.assertIn('active', accessory1_missing.get_attribute("class"))
        self.assertIn('active', accessory2_damaged.get_attribute("class"))
        self.assertIn('active', accessory3_damaged.get_attribute("class"))
        self.assertIn('active', accessory3_warranty.get_attribute("class"))

        #And the note is still there.
        self.assertEqual(note_text, note_box.text)



        #Confident that the results saved, he makes a change,
        accessory2_present = self.browser.find_element_by_id('id_accessories-1-status_1').find_element_by_xpath('..')
        accessory2_present.click()

        #And things update appropriately.

        self.assertIn('active', asset_present.get_attribute("class"))
        self.assertIn('active', accessory1_missing.get_attribute("class"))
        self.assertIn('active', accessory2_present.get_attribute("class"))
        self.assertIn('active', accessory3_damaged.get_attribute("class"))
        self.assertIn('active', accessory3_warranty.get_attribute("class"))

        #He hits Save & Print.
        self.browser.find_element_by_id('id_save_and_print').click()


        #Which spits out an exit ticket with the information and photos, if applicable on it.


        #Finally, he searches for the person's record again, and all appears as it should.
        self.fail('Finish the test')















#@TODO CHECK THAT CANCEL DOESN'T Trigger SAVE.
#@TODO Check Page
#@TODO From there he searches again on an asset
#@TODO From there he searches again on a person and gets a disambiguation list
