from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys
from ProcessManager.models import Asset, Person, Note, Assignment, DeviceType

# Create your tests here.

class LayoutAndStylingTest(FunctionalTest):
    def test_layout_and_styling(self):
        # Patsy goes go the home page
        self.browser.get(self.server_url)
        self.browser.set_window_size(1024, 768)

        #She notices that the input box is right justified
        inputbox = self.browser.find_element_by_id('id_search_box')

        self.assertAlmostEqual(inputbox.location['x'] + inputbox.size['width'],
                               1024,
                               delta=120)


        #Goes to look for an asset -- does it look nice?
        ##Create the asset to find.
        correct_asset = Asset.objects.create(tag=3011835,
                                             description='Another Macbook',
                                             wifi_address='aa:bb:cc:dd:12',
                                             bluetooth_address='aa:bb:cc:dd:13',
                                             type=self.create_type_for_assignment())
        self.browser.get(self.server_url)
        self.browser.set_window_size(1024, 768)

        #She notices that the input box is right justified
        inputbox = self.browser.find_element_by_id('id_search_box')
        inputbox.send_keys('3011835')
        inputbox.send_keys(Keys.ENTER)

        inputbox = self.browser.find_element_by_id('id_search_box')

        self.assertAlmostEqual(inputbox.location['x'] + inputbox.size['width'],
                               1024,
                               delta=120)

