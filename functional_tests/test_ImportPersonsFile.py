__author__ = 'matt'

from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys
from ProcessManager.models import Asset, Person, Note, Assignment, DeviceType
import csv
import time

class TechnicianImportPersonCSV(FunctionalTest):

    def test_can_upload(self):
        ##Tyler visits the website and clicks on the link for Imports.
        self.browser.get(self.server_url)
        self.browser.set_window_size(1024, 768)

        link = self.browser.find_element_by_id('id_import').click()

        import_url = self.browser.current_url

        ##Does this need to be improved?  ##Refactor to URL someday
        self.assertRegex(import_url, '/import')

        #The title says "Import Setup Information"
        self.assertEquals( 'Import Data Files', self.browser.title)

        ##And he sets the correct upload type
        self.browser.find_element_by_xpath('//*[@id="id_upload_type"]/option[text()="People"]').click()

        ##He sees multiple options, including one toward the top that offers to let him import a file.
        button = self.browser.find_element_by_id('id_select_person_upload')
       ## self.assertEqual('Choose File', button.text)

        ##He clicks on the button labeled "Select File to Upload"
        ##button.click()



        ##And chooses a file to upload.

        button.send_keys('/Users/matt/PycharmProjects/MLTIManager/functional_tests/test_files/Persons.csv')
        button.submit()

        ##That file then is uploaded, and a message is returned, stating that it was successful with the number of records imported..
        time.sleep(2)


        self.assertIn("Success. 1049 created, 97 updated, 0 failed.", self.browser.find_element_by_id('id_process_message').text)

        ##Not sure that that was true, he searches for a person, 'Abed'
        inputbox = self.browser.find_element_by_id('id_person_search_box')
        inputbox.send_keys('Abed')
        inputbox.send_keys(Keys.ENTER)
        ##And finds the appropriate records.
        person_url = self.browser.current_url
        self.assertRegex(person_url, '/person/\d+/')

        ##Satisfied, he leaves, happy.

        ##Test Unsuccessful Use Case##

    def test_returns_the_correct_number_of_uploaded_records(self):
        ##Tyler visits the website and clicks on the link for Imports.
        self.browser.get(self.server_url)
        self.browser.set_window_size(1024, 768)

        link = self.browser.find_element_by_id('id_import').click()

        import_url = self.browser.current_url


        #The title says "Import Setup Information"
        self.assertEquals( 'Import Data Files', self.browser.title)

        ##And he sets the correct upload type
        self.browser.find_element_by_xpath('//*[@id="id_upload_type"]/option[text()="People"]').click()

        ##He sees multiple options, including one toward the top that offers to let him import a file.
        button = self.browser.find_element_by_id('id_select_person_upload')

        ##He clicks on the button labeled "Select File to Upload"
        ##button.click()
        ##And chooses a file to upload.

        #Establish a person to change.
        Person.objects.create(first_name='z_Abed',
                              last_name='z_Farris',
                              student_number='130130220')

        with open('functional_tests/test_files/generated_files/f_Persons.csv', 'w') as inFile:
            tempCSV = csv.writer(inFile)
            # Abed,Faris,MiddleName,130130220,7/22/98 00:00
            tempCSV.writerow(['Last Name','First Name', 'Middle Name', 'student number', 'dob'])
            tempCSV.writerow(['Abed', 'Faris', 'M', '130130220','1992-02-01'])
            tempCSV.writerow(['Morgan', 'Rachel', 'M', '130130210','1999-09-01'])
            tempCSV.writerow(['Binks', 'JarJar', 'M', '130130221','1994-10-02'])


        button.send_keys('/Users/matt/PycharmProjects/MLTIManager/functional_tests/test_files/generated_files/f_Persons.csv')
        button.submit()

        ##Wait for upload
        time.sleep(2)

        ##
        self.assertIn("Success. 2 created, 1 updated, 0 failed.", self.browser.find_element_by_id('id_process_message').text)




    def test_returns_useful_failure_message(self):
        ##Tyler visits the website and clicks on the link for Imports.
        self.browser.get(self.server_url)
        self.browser.set_window_size(1024, 768)

        link = self.browser.find_element_by_id('id_import').click()

        button = self.browser.find_element_by_id('id_select_person_upload')



        button.send_keys('/Users/matt/PycharmProjects/MLTIManager/functional_tests/test_files/invalid_Persons.csv')
        button.submit()



        #Which fails, giving him a semi-useful message about the failure.
        self.assertNotIn("Success - 5 records imported.", self.browser.find_element_by_id('id_process_message').text)

        self.assertIn("1 failed.",self.browser.find_element_by_id('id_process_message').text)


